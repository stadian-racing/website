# Stadian Racing Community Website

This is a static website hosted by GitLab. Please report any issues you find with the site here.

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. Install [Hugo](https://gohugo.io/getting-started/installing/)
1. Preview your project: `hugo server`
1. Add content

### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313`.

## Thanks

Special thanks to [adityatelange](https://github.com/adityatelange/hugo-PaperMod/) for their work on the Theme [Paper Mod](https://github.com/adityatelange/hugo-PaperMod/) which this site is based upon.
