---
title: "Community Rules"
keywords: ["stadia","stadian","racing","community", "rules"]
url: /rules/
description: General community rules
---

### Problematic Rules
1. Use of uncommon and unfamiliar chat abbreviations

2. Public begging, complaining and smart-alecky posts (DM a staff member instead)

3. Advertising for other sites/discord servers (Permission must be requested from a Staff member)

4. Excessive use of role-tags or quoting messages with role-tags (tags can be removed before sending message)

5. Unannounced absence and unreliable participation in events you signed up for beforehand

### Prohibited Rules
6. Spamming or flooding a channel with messages

7. Intrusive self-promoting

8. Promoting the use of cheats, bugs, exploits (handling of these will be decided by admin or hosts)

9. Promoting and/or calling for revenge and/or intentional dirty behaviour against other players

10. Linking to illegal or malicious content, websites, social media posts and other stuff

11. Explicit sexual content of any kind (videos, images, texts, avatars, nicknames)

12. Insulting in a way that is personally degrading and/or discriminating against individuals and/or social groups