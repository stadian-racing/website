---
title: "Rules of Clean Racing"
keywords: ["stadia","stadian","racing","community", "rules", "clean racing"]
description: Basic rules and etiquette expected when racing in SRC Events
showTOC: true
url: /rocr/
---

## 1. Principles
Always drive responsibly and respectfully. Clean racing offers the best experience for all our racers. The ROCR is intended to promote the spirit of competition by providing a level playing field. For the extra care, attention and patience, we all get far more – action, entertainment, realism, progression and reward – in return.

<!--more-->
---
## 2. Racing core
### 2.1. Car control
Drivers are responsible for keeping their cars under control, driving within one’s own limits and those of the given car / track / weather combination.
### 2.2. Positional awareness
Stay aware of drivers nearby so that those alongside can be given space.
Use proximity arrows, mirrors and look around. Take extra care while using limited view cameras. Advanced drivers may use reduced HUD settings provided they can do so safely.
### 2.3. Leaving space
Cars alongside should be left a car width plus some tolerance to the edge of the track.
What is considered alongside can vary in different circumstances (see ‘Battling for position’) but when in doubt, leave space.
Make an allowance for speed differences and game inconsistencies – e.g. latency and camera offset.
### 2.4. Avoiding contact
Drivers should try to avoid any contact with other cars. Ensure bumps or scrapes are the exception not the norm.
### 2.5. Avoiding incidents
By striving to avoid contact, stay aware and keep control, we drastically reduce the likeliness and severity of incidents for ourselves and others.

---
## 3. Battling for position
### 3.1. Overtaking
It’s the responsibility of the attacker to overtake safely or await a better opportunity. Attempt passes only if there is a minimum chance of contact.
The attacker has the best view as everything is ahead of them. Be aware that the defender could make a misjudgment or mistake which blocks the desired line.
The defender should avoid late or sudden movements.
### 3.2. Clean passes
Where a car is clearly fast enough on the straight to fully complete the overtake before the next corner, no move should be made to defend the position.
### 3.3. Battling on straights
Cars are considered alongside and entitled to space when any part of their car is alongside.
The defender may choose and commit to a defensive line provided the attacker is allowed enough time to react and contact is unlikely. Remember, regular defending costs time allowing more cars to catch up and those in front to get away.
Avoid moving across the passed car until the move is clearly complete.
### 3.4. Battling on corners
Cars are considered alongside and entitled to space when the front wheels of one are inline or ahead of the rear wheels of the other.
If the attacker is not alongside before the braking zone, the defender has the right to the apex. Unless the defender makes a clear mistake, the attacker should hold back and await a better opportunity.
Avoid sudden changes of direction in the braking zone as other drivers are already committed to their lines.

---
## 4. Racing events
### 4.1. Race starts
Take extra care while the field is compact and be prepared to slow down at any time. Apparent gaps may close quickly going into the braking zones. Maintain a gap ahead for visibility and reaction times.
Most tracks allow two lines of cars through the first corners. Drivers are strongly advised to take a position in one of these lines before the braking zone(s), holding this line until there is space. Moving outside these lines can be risky.
### 4.2. Bunching up
Braking points, lines and cornering speeds tend to vary, drivers following another closely should leave enough gap ahead to avoid getting caught out.
Busy corners or battling drivers ahead travel slower than usual, drivers behind should adjust their speed as necessary.
### 4.3. Rejoining the track
Drivers should rejoin smoothly and safely without impeding other drivers, even if this means waiting.
### 4.4. Being lapped
Drivers being lapped by the lead car(s) should move over as soon as it’s safe to do so.
### 4.5. Unfair gains
Drivers who gain a position by unfair means should slow down and allow the cars to regain their rightful position.
### 4.6. Mistakes and collisions
In race situations, misjudgments and mistakes will happen, drivers are expected to learn from these and make adjustments for the rest of the race and beyond.
Drivers involved in an incident should take a clip. All may not be as it seems so keep calm and carry on.

---
## 5. Rule enforcement
### 5.1. Agreeing to the rules
By participating in the SRC events all drivers indicate that they agree to the rules here.
### 5.2. Streaming and clips
Streaming your event races is extremely helpful both for learning and sharing perspective. When not streaming, take a clip of any incidents.
### 5.3. Discussion
Discuss racing issues in a respectful and open-minded way. Sometimes there is a good explanation or a completely different perspective.
We all want to learn from mistakes and race cleaner. Just don’t forget to share the good stuff too!
Raise any regular or major concerns with a host.
### 5.4. Warnings and penalties
While hosts will do their best to guide drivers and avoid the situation reaching this stage, continued regular infractions will trigger warnings, penalties or suspensions to protect the community.


Good luck and see you on the asphalt!
