---
title: Redfox
subtitle: Host, Racer.
keywords: ["stadia","stadian","racing","community", "host"]
tags: ["Host"]
author: Redfox
description: Greetings! Redfox here.
weight: 7
stadia: Redfox#2355
layout: team
---

### How I got here…

I stumbled into Grid by chance, as a welcome addition to the Stadia Pro catalog in March 2020.  I quickly became fascinated with the challenge of reducing available driving assists, while endeavoring to remain competitive with others online.  By the time I turned traction and stability control all the way off, I was completely hooked, and realized this driving game was much deeper than it initially appeared.

In the early days, the regular clean and fast racers in the driver pool, quickly recognized each other for displaying commendable on-track demeanor and etiquette.  Each looking to race with likeminded drivers more often, a then more selective, and much lesser known, version of the Stadian Racing Community was born.  

Proudly racing with the SRC since inception, I’ve welcomed many new members to the paddock, as our small group of less than a dozen, has flourished into hundreds; soon to be many more.

Now, widely regarded as the leader in racing on Stadia, the spirit of the early era lives on, with the SRC’s established tradition of respectful competitive racing.  The modern SRC, has become THE place to be; THE place the best drivers on Stadia call home…  Welcome, to the SRC.

### What I’m up to now…

With the intent of providing fun multiplayer content to Stadia racing enthusiasts, I work with the team to coordinate competitive events and series for our members to enjoy.  

Planning cars and tracks for race day, coordinating driver and division rosters, scheduling participants across 9 time zones, organizing practice and race sessions, building spreadsheets to tally results, publishing the post-race write ups to announce our winners… all keep me occupied when not tearing up the asphalt in public lobbies, hosted by myself and others.

With many hours logged in Grid, I’m happy to share my experience with newcomers, helping configure game setting to maximize their enjoyment and gameplay experience.  I also enjoy trading tips, tricks, strategy, and race-craft coaching, with any that seek to improve their on-track performance, as I too, am always learning from the best among us here.

While my default demeanor speaks more Sunday Driver, than Race Car Driver, I love getting sweaty-palm levels of fired-up, for all-out white-knuckle wheel-to-wheel sprints to the checkered flag.  While always cheering on the fastest among us, perhaps more importantly, I have a deep-rooted appreciation for those that drive with respect for rival competitors while on the circuit.  

I’m honoured to be part of the SRC team, who tireless strives to provide the best racing experiences on Stadia.  

We can’t wait to see you on the track.

“Slow is smooth, smooth is fast.”

Redfox

{{< image src="redfox.gif" >}}