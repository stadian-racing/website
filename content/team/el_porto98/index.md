---
title: El_Porto98
subtitle: Host, Racer.
keywords: ["stadia","stadian","racing","community", "host"]
tags: ["Host"]
author: El_Porto98
weight: 8
layout: team
stadia: ELPorto98#6199
---
Well, hello. 

I'm El_Porto98. 

The whole stadia story began in April 2020 for me. My brother (Cabo) invited me to play grid together on this platform. I happily started playing with him and he told me I maybe could join the ongoing championship in this community. From that moment I was hooked in this awesome community. 

We have fast reliable racers. It’s just amazing battling them.

{{< image src="elporto.jpg" >}}