---
title: Hellmonkey88
subtitle: Admin and Content Lead.
keywords: ["stadia","stadian","racing","community", "moderator"]
tags: ["Admin", "Moderator", "Host"]
author: Hellmonkey88
weight: 1
layout: team
stadia: HellMonkey88#3687
---

Hello. 

I’m Scott AKA HellMonkey88. 

After joining the Stadian Racing Community at the beginning of 2020 I invested myself into creating and hosting events for all our members. My passion to drive this community forward has only grown since then.

As one of the administrators on the discord server I have tried to create a warm and friendly atmosphere where people can come enjoy clean friendly competitive racing.

I have enjoyed racing games across many platforms over the years. 

Working as a vehicle technician as a day job and having a keen interest in motorsports has helped me keep up my love affair with racing games.

When i’m not creating new ideas for events i’m hosting, commentating and creating content on our YouTube channel.
I have the privilege of working alongside a great team here in the SRC and I really hope you can enjoy and appreciate all our hard work and dedication.


