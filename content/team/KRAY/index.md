---
title: KRAY
subtitle: Moderator, Racer.
keywords: ["stadia","stadian","racing","community", "moderator","host"]
tags: ["Moderator", "Host"]
author: "KRAY"
weight: 4
layout: team
stadia: Kray#2432
---

I am KRAY.

I am one of the early members to join the SRC and have remained an active participant in events since I joined.
 
When I’m not regularly racing in GRID, I help organise and host events for the SRC. 

As a Mod of the SRC, I help provide a safe and clean environment on the server for our 400+ members. 

I also produce promotional content for the community including branding, events posters and videos to add a little magic to the event announcements. 

As a long standing GRID player with over 1000 hours played so far, I have come to know many tips, tricks and advice for different cars and tracks to make people a cleaner and faster racer. - I’m always happy to offer help where required. 

Most importantly I hope all our members enjoy what the SRC has to offer, with the ultimate aim of having fun whilst feeling welcome in a friendly and safe environment. 

See you on the track!

“Let's Go!”

{{< image src="kray.jpg" >}}
