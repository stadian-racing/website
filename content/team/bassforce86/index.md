---
title: bassforce86
tags: ["Admin", "Moderator"]
author: "Bassforce86"
layout: team
weight: 2
---

Hello all, I'm Bassforce86. Generally you won't see me in the lobbies and events. What I do is more "behind the scenes" things.

I help manage the discord, in terms of roles, permissions, and general setup. I built and maintain this site for the community. And generally attempt to keep things rolling in the right directions from a Tech point of view. 