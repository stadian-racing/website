---
title: ilseum
subtitle: Host, Racer.
keywords: ["stadia","stadian","racing","community", "moderator"]
tags: ["Moderator"]
author: ilseum
weight: 3
layout: team
---
Heyhey, I'm ilseum. I'm around since day 1 of both Stadia and this Community.

I run the Twitter account [@StadianRacing](https://twitter.com/StadianRacing) for the SRC and do a little Discord moderation if needed. You'll usually find me in GRID and Trials Rising events.

That's your 280 characters about me :slightly_smiling_face: