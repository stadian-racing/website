---
title: Xotic
subtitle: Host, Racer.
keywords: ["stadia","stadian","racing","community", "host"]
tags: ["Host"]
author: Xotic
weight: 9
layout: team
---

Mainly hosts [F1 2020](/events/f1-2020)