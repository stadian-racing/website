---
title: JayLdn
subtitle: Host, Racer.
keywords: ["stadia","stadian","racing","community", "host"]
tags: ["Host"]
author: "JayLdn"
weight: 6
stadia: Jay#6614
layout: team
---
Hey, it's Jay. 

Like most the team, I love clean hard racing. I'd actually rather be the cleanest than fastest, but then again, why choose!?

Fast approaching the landmark of driving the circumference of the globe on GRID, I'm also about to embark on my F1 journey. Got a lot of experience racing alongside the best on Stadia and happy to share knowledge, skills, tips and tricks picked up along the way.

New to the team and hoping to ease the load as the community grows. You can find me on track, Discord and YT – where most my racing is live streamed, especially the SRC events.

See you on the grid!

Jay(Ldn)
