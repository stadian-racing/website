---
title: B1GDAVE
subtitle: Host, Racer.
keywords: ["stadia","stadian","racing","community", "host"]
tags: ["Host"]
author: "B1GDAVE"
weight: 5
layout: team
stadia: B1GDAVE#7099
---

Hi there I am B1GDAVE,

I joined the SRC sometime in July 2020 and because I love the community so much i recently became a host running the Global Masters Championship along side Kray.

I also help out here and there on the server and regularly post events and promotions on Reddit.

One of my reasons for joining the SRC was watching a YouTube GRID video of Hellmonkeys, his familiar Scottish accent and the laughs and clean racing helped me push the button to join. I’ve never looked back!!

My main game at the moment is GRID but I have recently started playing F12020 so will be competing very soon, I also dabble socially with Trials Rising which can be so much fun.

I will continue to help push the SRC community along as we all try to make it the best place to be when it comes to racing on Stadia, not only as a clean racing environment but also a fun safe place to be for our members.

Happy racing everyone and I’ll see you on the track!!

B1GDAVE

{{< figure src="b1gdave.gif" >}}