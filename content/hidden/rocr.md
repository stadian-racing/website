---
title: "Rules of Clean Racing (ROCR)" # in any language you want
keywords: ["stadia","stadian","racing","community", "rules", "clean racing"]
description: Basic rules and etiquette expected when racing in SRC Events
---

We are all familiar with the standards of SRC league races, but for some of our newest members or those who just need a little reminding.

This is clean racing only to compete in a fair and honest manner.

Sometimes the odd bump or scrape can’t be helped as we all make mistakes as we travel fast and very close to each other, sometimes bumper to bumper, it happens - If it’s not too excessive, it’s fine.

No ramming, crashing or torpedoing into your opponent deliberately - You must drive responsibly, in a way that will not ruin the race and enjoyment for others.

No forcing or barging your way through a large pack of bunched up cars which may result in crashes, leaning or putting other drivers off the track (especially on turn 1, lap 1) - Be patient and slow down, the right opportunity to move up position will come as the race continues.

Hold your racing line, if you’re about to be overtaken and the car is going to make a clear clean pass, do not abruptly divert from your line in an attempt to deliberately block and obstruct - Show sportsmanship and accept you’re being overtaken. It will be your job to regain your position through a clean racing battle which will make the experience better for everyone.

If you go off the track, please check if there are any fast moving cars about to pass, these drivers have right of way until you rejoin safely. - This will avoid any unnecessary crashes when re-joining the track.

Be cautious when approaching a corner of other drivers around you who may be by your sides, if you don’t have the right of way racing line, the other driver should be left room to avoid any crashes or being leaned on.

If you gain a position by unfair means, eg, cutting across the grass and gaining one or two positions, you must slow down and allow the cars you have passed to regain there rightful position.

Use your rear view mirror, use your side and back look buttons and be conscious of the proximity display arrows - Especially if using the hood cam, field of view can be limited as opposed to chase cam so it’s important to be aware who is around you.

If you’re racing at the back of the field and you’re in the unfortunate position of being lapped by the lead driver and so on, it is asked that you show courtesy and allow those drivers to pass.

Most importantly, have fun.
