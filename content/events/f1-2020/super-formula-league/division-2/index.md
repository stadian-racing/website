---
# When your event is ready, set this to false and it will show
draft: false
title: Division 2
aliases: ["division-2"]
tags: ["F1 2020", "Super Formula"] 
author: Scanline
description: "F2 2020 cars: equal performance"
expiryDate: 2021-04-25T21:00:00+00:00
weight: 2
---
Qualifying
: 1 shot

Feature Race
: 50%

Sprint Race
: 5 Laps
----

**5 races**: Austria, Brazil, Singapore, Netherlands, Mexico

<!--more-->
## Points system:

### Feature race:

1st
: 25

2nd
: 18

3rd
: 15

4th
: 12

5th
: 10

6th
: 8

7th
: 6

8th
: 4

9th
: 2

10th
: 1

&nbsp;
> **Note**: The top ten drivers score points with two points being awarded to the driver who set the fastest lap of the race.

---

### Sprint Race:

1st
: 15

2nd
: 12

3rd
: 10

4th
: 8

5th
: 6

6th
: 4

7th
: 2

8th
: 1

&nbsp;
> **Note**: The top eight finishers score points and the driver who sets the fastest lap scores two points. Any driver who is not classified in the top ten positions at the end of the race, will not be eligible for points awarded for fastest lap.

----

## Settings

### Session
{{< image src="f2_session_options.jpg" linkFullImage="true" >}}
### Race
{{< image src="f2_race_settings.jpg" linkFullImage="true" >}}
### Assists
{{< image src="f2_assist_restrictions.jpg" linkFullImage="true" >}}
