---
title: "Brazil"
hideSummary: true
tags: ["F1 2020", "Super Formula League"] 
keywords: ["racing","event", "f1 2020"]
layout: "event"
expiryDate: 2021-01-24T21:30:00+00:00
event:
    start: 2021-01-24T19:30:00+00:00
    duration: 2h
    show_calendar_link: true
weight: 4
cover:
  image: tracks/brazil/interlagos.png
---

Both Division 1 & Division 2 races will be run at the same time.
