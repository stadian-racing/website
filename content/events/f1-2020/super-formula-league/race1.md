---
title: "Austria"
hideSummary: true
keywords: ["racing","event", "f1 2020"]
tags: ["F1 2020", "Super Formula League"] 
layout: "event"
expiryDate: 2021-01-10T21:30:00+00:00
event:
  start: 2021-01-10T19:30:00+00:00
  duration: 2h
  show_calendar_link: true
weight: 3
cover:
  image: tracks/austria/redbull-ring.png
---

Both Division 1 & Division 2 races will be run at the same time.

