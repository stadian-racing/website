---
title: "Mexico"
hideSummary: true
tags: ["F1 2020", "Super Formula League"] 
keywords: ["racing","event", "f1 2020"]
layout: "event"
expiryDate: 2021-03-07T21:30:00+00:00
event:
    start: 2021-03-07T19:30:00+00:00
    duration: 2h
    show_calendar_link: true
weight: 7
cover:
  image: tracks/mexico/autodromo-hermanos-rodriguez.png
---

Both Division 1 & Division 2 races will be run at the same time.
