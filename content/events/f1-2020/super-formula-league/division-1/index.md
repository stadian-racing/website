---
# When your event is ready, set this to false and it will show
draft: false
title: Division 1
aliases: ["division-1"]
tags: ["F1 2020", "Super Formula"] 
author: Hellmonkey88
description: "F1 2020 cars: equal performance"
expiryDate: 2021-04-25T21:00:00+00:00
weight: 1
---

Qualifying
: 18 minute

Race
: 50%

---
**5 races**: Austria, Brazil, Singapore, Netherlands, Mexico

<!--more-->
## Points system:

1st
: 25

2nd
: 18

3rd
: 15

4th
: 12

5th
: 10

6th
: 8

7th
: 6

8th
: 4

9th
: 2

10th
: 1

&nbsp;

> **Note**: 1 point awarded for fastest lap

----

## Settings

### Session
{{< image src="f1_session_options.jpg" linkFullImage="true" >}}
### Race
{{< image src="f1_race_settings.jpg" linkFullImage="true" >}}
### Assists
{{< image src="f1_assist_restrictions.jpg" linkFullImage="true" >}}
