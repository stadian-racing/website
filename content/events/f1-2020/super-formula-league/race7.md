---
title: "Bahrain"
hideSummary: true
tags: ["F1 2020", "Super Formula League"] 
keywords: ["racing","event", "f1 2020"]
layout: "event"
expiryDate: 2021-04-25T21:30:00+0000
event:
    start: 2021-04-25T19:30:00+0000
    duration: 2h
    show_calendar_link: true
weight: 9
cover:
  image: tracks/bahrain/bahrain.png
---

Both Division 1 & Division 2 races will be run at the same time.
