---
title: Wednesday League
author: Spaceman37
tags: ['F1 2020']
description: Best suited to North American Racers
layout: events
stadia: Spaceman37#1536
event:
    start: 2021-06-03T01:30:00-00:00
    recurring: Weekly
    duration: 1h
    show_calendar_link: true
---

> *The time may display as Thursday because all times shown on the website are in UTC. Do not worry, should you wish to add the event to your google calendar, it will be added at the correct time.*

## Event Times
- 21:30 EDT
- 18:30 PDT

## Format
Car
: F1 cars

Sprint Race
: One-Shot Qualify, 5 laps 

Feature Race
: 25% reverse grid

---
You must add Spaceman37#1536 as a friend on Stadia so that you can be invited to the private lobby.
