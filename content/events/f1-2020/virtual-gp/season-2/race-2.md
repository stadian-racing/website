---
title: Circuit Zandvoort
author: Xotic
stadia: Xotic#2566
layout: event
description: Race
tags: ['F1 2020', 'Virtual GP', 'Season 2']
expiryDate: 2021-08-26T21:30:00+00:00
weight: 6
event:
  start: 2021-08-26T20:30:00+00:00
  duration: 90m
  show_calendar_link: true
---

## General Info

Feature Length
: 50%

Formation Lap
: Yes

Grid
: [Qualifying](/events/f1-2020/virtual-gp/season-2/race-2-q) Results

