---
title: Circuit de Spa-Francorchamps 
author: Xotic
stadia: Xotic#2566
layout: event
tags: ['F1 2020', 'Virtual GP', 'Season 2']
expiryDate: 2021-09-28T21:30:00+00:00
weight: 15
event:
  start: 2021-09-28T20:30:00+00:00
  duration: 1h
  show_calendar_link: true
---

## General Info
Qualifying
: One-shot

Feature Length
: 50%

