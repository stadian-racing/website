---
title: Autodromo Jose Carlos Pace 
author: Xotic
stadia: Xotic#2566
layout: event
tags: ['F1 2020', 'Virtual GP', 'Season 2']
expiryDate: 2021-10-12T21:30:00+00:00
weight: 19
event:
  start: 2021-10-12T20:30:00+00:00
  duration: 1h
  show_calendar_link: true
---

## General Info

Qualifying
: One-shot

Feature Length
: 50%

