---
title: Practice Session 1
author: Xotic
stadia: Xotic#2566
description: All 7 tracks will be run in this session
layout: event
tags: ['F1 2020', 'Virtual GP', 'Season 2']
expiryDate: 2021-08-10T21:30:00+00:00
weight: 1
event:
  start: 2021-08-10T20:30:00+00:00
  duration: 2h
  show_calendar_link: true
---

# Tracklist

- Autódromo Hermanos Rodríguez, Mexico :flag_mx:
- Circuit Zandvoort, Netherlands :flag_nl:
- Autodromo Nazionale Monza, Italy :flag_it:
- Circuit de Monaco, Monaco :flag_mc: 
- Red Bull Ring, Austria :flag_at: 
- Baku City Circuit, Azerbaijan :flag_az: 
- Circuit de Spa-Francorchamps, Belgium :flag_be: 
- Marina Bay Street Circuit, Singapore :flag_sg: 
- Autodromo Jose Carlos Pace, Brazil :flag_br: 
- Silverstone Circuit, United Kingdom :flag_gb: