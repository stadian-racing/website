---
title: Autodromo Nazionale Monza
author: Xotic
stadia: Xotic#2566
layout: event
description: Race
tags: ['F1 2020', 'Virtual GP', 'Season 2']
expiryDate: 2021-09-02T21:30:00+00:00
weight: 8
event:
  start: 2021-09-02T20:30:00+00:00
  duration: 90m
  show_calendar_link: true
---

## General Info

Qualifying
: One-shot

Feature Length
: 50%


