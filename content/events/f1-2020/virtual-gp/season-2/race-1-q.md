---
title: Autódromo Hermanos Rodríguez 
author: Xotic
stadia: Xotic#2566
layout: event
tags: ['F1 2020', 'Virtual GP', 'Season 2']
expiryDate: 2021-08-17T21:30:00+00:00
weight: 3
event:
  start: 2021-08-17T20:30:00+00:00
  duration: 1h
  show_calendar_link: true
---

## General Info

Feature Length
: 25%

Sprint Length
: 5 laps

