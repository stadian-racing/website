---
title: Practice Session 1
author: Scanline
stadia: Scanline#6792
description: All 7 tracks will be run in this session
layout: event
tags: ['F1 2020', 'Virtual GP', 'Season 1']
expiryDate: 2021-06-01T21:30:00+00:00
event:
  start: 2021-06-01T20:00:00+00:00
  duration: 2h
  show_calendar_link: true
---

# Tracklist

 - Circuit de Barcelona-Catalunya, Spain
 - Circuit Zandvoort, Netherlands
 - Yas Marina Circuit, Abu Dhabi
 - Circuit de Spa-Francorchamps, Belgium
 - Autodromo Jose Carlos Pace, Brazil
 - Melborne Grand Prix Circuit, Australia 
 - Red Bull Ring, Austria