---
title: Red Bull Ring
author: Scanline
stadia: Scanline#6792
layout: event
description: Race
tags: ['F1 2020', 'Virtual GP', 'Season 1']
expiryDate: 2021-07-22T21:30:00+00:00
weight: 16
event:
  start: 2021-07-22T20:00:00+00:00
  duration: 90m
  show_calendar_link: true
cover:
  image: events/f12020/virtual-gp/VGPSR7.jpg
---

## General Info

Feature Length
: 50%

Formation Lap
: Yes

Grid
: [Qualifying](/events/f1-2020/virtual-gp/season-1/race-7-q) Results

