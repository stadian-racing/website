---
title: Circuit Zandvoort | Qualifying
author: Scanline
stadia: Scanline#6792
layout: event
tags: ['F1 2020', 'Virtual GP', 'Season 1']
expiryDate: 2021-06-15T21:30:00+00:00
weight: 5
event:
  start: 2021-06-15T20:00:00+00:00
  duration: 1h
  show_calendar_link: true
cover:
  image: events/f12020/virtual-gp/VGPSR2.jpg
---

## General Info

Feature Length
: 25%

Sprint Length
: 5 laps

