---
title: Melborne Grand Prix Circuit
author: Scanline
stadia: Scanline#6792
layout: event
description: Race
tags: ['F1 2020', 'Virtual GP', 'Season 1']
expiryDate: 2021-07-15T21:30:00+00:00
weight: 14
event:
  start: 2021-07-15T20:00:00+00:00
  duration: 90m
  show_calendar_link: true
cover:
  image: events/f12020/virtual-gp/VGPSR6.jpg
---

## General Info

Feature Length
: 50%

Formation Lap
: Yes

Grid
: [Qualifying](/events/f1-2020/virtual-gp/season-1/race-6-q) Results

