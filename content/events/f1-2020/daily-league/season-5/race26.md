---
draft: false
title: Circuit Paul Ricard
tags: ["F1 2020", "Daily League", "Season 5"]  
keywords: ["racing","event", "f1 2020"]
author: Scanline
layout: event
expiryDate: 2021-03-19T22:00:00+00:00
event:
  start: 2021-03-19T21:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 26
cover:
  image: tracks/france/paul-ricard.png
---

Race 26 will be held at the Circuit Paul Ricard in France, the famous 5.8 km track that is east of Marseille. 

15 corners here - 6 to the left and 9 to the right - with the main overtaking chance expected going into turn 8. Top speeds should be around 330 kph.