---
draft: false
title: Autodromo Nazionale Monza
tags: ["F1 2020", "Daily League", "Season 5"]  
keywords: ["racing","event", "f1 2020"]
author: Scanline
layout: event
expiryDate: 2021-03-09T22:00:00+00:00
event:
  start: 2021-03-09T21:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 18
cover:
  image: tracks/italy/monza.png
---

Race 18 takes us to Italy where Monza's reputation as one of the fastest circuits in the sport is well earned. We have 11 corners on this 5.8km track, with the best overtaking chance coming into the heavy braking zone of the turn 1 chicane.
