---
draft: false
title: "Bahrain International Circuit (Short)"
tags: ["F1 2020", "Daily League", "Season 5"] 
keywords: ["racing","event", "f1 2020"]
author: Scanline
layout: event
expiryDate: 2021-02-22T22:00:00+00:00
event:
  start: 2021-02-22T21:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 6
cover:
  image: tracks/bahrain/bahrain.png
---

Race 6 takes us around the 5.4 kilometres of the magnificent Bahrain International Circuit, with 15 corners and 2 good passing opportunities into turns 1 and 4. Keep an eye out for drivers locking the front left tyre into the tricky braking zone of turn 10. 