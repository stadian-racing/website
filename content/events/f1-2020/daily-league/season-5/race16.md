---
draft: false
title: "Red Bull Ring"
tags: ["F1 2020", "Daily League", "Season 5"]  
keywords: ["racing","event", "f1 2020"]
author: Scanline
hideSummary: true
layout: event
expiryDate: 2021-04-02T22:00:00+00:00
event:
  start: 2021-04-02T21:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 36
cover:
  image: tracks/austria/redbull-ring.png
---

 Will be held at the Spielberg Circuit (Red Bull Ring) in Austria which is situated 700 metres above sea level, with just 10 corners making up one of the shortest laps of the season. One time around here is a distance of 4.2 kilometres, with the best overtaking chances into turn 1, or the tight, uphill turn 3.