---
draft: false
title: Circuit Zandvoort
tags: ["F1 2020", "Daily League", "Season 5"]  
keywords: ["racing","event", "f1 2020"]
author: Scanline
layout: event
expiryDate: 2021-03-15T22:00:00+00:00
event:
  start: 2021-03-15T21:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 22
cover:
  image: tracks/netherlands/zandvoort.png
---

Race 22 will be held at the Zandvoort Circuit in the Netherlands. 4 lefts and 10 rights make up the 14 corners of the narrow and demanding Zandvoort Circuit, with plenty of peaks and valleys over the course of the 4.2 kilometre lap, which will demand absolute concentration from the drivers here today.