---
draft: false
title: Circuit de Barcelona-Catalunya
tags: ["F1 2020", "Daily League", "Season 5"]  
keywords: ["racing","event", "f1 2020"]
author: Scanline
layout: event
expiryDate: 2021-03-12T22:00:00+00:00
event:
  start: 2021-03-12T21:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 21
cover:
  image: tracks/spain/circuit-de-catalunya.png
---

Race 21 takes us to the Circuit de Barcelona-Catalunya in Spain, a track that will certainly force the drivers to push themselves. 

It consists of a very impressive main straight going into turn 1. It's a straight that also offers a DRS zone - so it's likely to be a hot spot for overtakes. 
