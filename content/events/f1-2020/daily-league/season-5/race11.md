---
draft: false
title: Hanoi Circuit
tags: ["F1 2020", "Daily League", "Season 5"] 
keywords: ["racing","event", "f1 2020"]
author: Scanline
layout: event
expiryDate: 2021-03-01T22:00:00+00:00
event:
  start: 2021-03-01T21:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 11
cover:
  image: tracks/vietnam/fia/hanoi.jpg
---

Race 11 will be held at the Hanoi Track in Vietnam.

It's a track that combines the bespoke design of a traditional race track with the tight, close barriers of a street circuit.  23 corners and a total distance of 5.5 kilometres. 

Watch out in particular for overtakes into the braking zone at turn 11.