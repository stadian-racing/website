---
draft: false
title: Shanghai International Circuit
tags: ["F1 2020", "Daily League", "Season 5"]  
keywords: ["racing","event", "f1 2020"]
author: Scanline
layout: event
expiryDate: 2021-03-22T22:00:00+00:00
event:
  start: 2021-03-22T21:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 27
cover:
  image: tracks/china/shanghai.png
---
 
For Race 27 we'll be in the Yangtze river delta home of the 16 corners that make up the Shanghai International Circuit. 

54% of this 5.3 kilometre lap is taken at full throttle, and we'll be getting up to speeds of around 322 kph with DRS assistance down the back straight before they brake into the tight hairpin at turn 14.