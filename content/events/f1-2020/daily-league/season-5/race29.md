---
draft: false
title: Silverstone Circuit (Normal)
tags: ["F1 2020", "Daily League", "Season 5"]  
keywords: ["racing","event", "f1 2020"]
author: Scanline
expiryDate: 2021-03-24T22:00:00+00:00
layout: event
event:
  start: 2021-03-24T21:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 29
cover:
  image: tracks/uk/silverstone.png
---

 We will be at the 5.8 kilometre long Silverstone Circuit, this event is one of the longest of the season, with 18 corners in the current layout. 
 
 With average lap speeds reaching around 233 kph , it's also one of the quickest tracks in the sport; watch for cars taking the right-handers of the Abbey (Turn 1) and Copse (Turn 9) flat out.
