---
draft: false
title: Circuit of the Americas (Normal)
tags: ["F1 2020", "Daily League", "Season 5"]  
keywords: ["racing","event", "f1 2020"]
author: Scanline
layout: event
expiryDate: 2021-03-31T22:00:00+00:00
event:
  start: 2021-03-31T21:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 34
cover:
  image: tracks/us/austin.png
---

The penultimate race of the season (34) will feature 10 turns to the left and 10 to the right, here at the fantastic Circuit of the Americas. 

Overtaking opportunities into turn 1 and turn 12 at this anti-clockwise 5.8 kilometre track, and we should see the average lap speeds of around 203 kph.