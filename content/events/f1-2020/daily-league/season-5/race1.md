---
draft: false
title: Melbourne Grand Prix Circuit
tags: ["F1 2020", "Daily League", "Season 5"] 
keywords: ["racing","event", "f1 2020"]
author: Scanline
layout: event
expiryDate: 2021-02-15T22:00:00+00:00
event:
  start: 2021-02-15T21:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 1
cover:
  image: tracks/australia/melborne.png
---

5.3 kilometres of public roads - closed for the weekend of course - make for a bumpy surface with little undulation. There are 16 corners around the lake, with the best passing opportunities coming at turn 1 and turn 3.