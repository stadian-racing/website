---
draft: false
title: Circuit de Monaco
tags: ["F1 2020", "Daily League", "Season 5"]  
keywords: ["racing","event", "f1 2020"]
author: Scanline
layout: event
expiryDate: 2021-03-11T22:00:00+00:00
event:
  start: 2021-03-11T21:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 20
cover:
  image: tracks/monaco/monte-carlo.png
---

Race 20 brings us to the prestigious Circuit de Monaco. 

It's 2 miles and 19 corners through the streets of Monte Carlo, and although the average lap speed of around 150 kph is the lowest of the season, the tiny margins for error make it the natural habitat for self destruction :)