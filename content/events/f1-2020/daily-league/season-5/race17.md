---
draft: false
title: Baku City Circuit
tags: ["F1 2020", "Daily League", "Season 5"] 
keywords: ["racing","event", "f1 2020"]
author: "Scanline"
hideSummary: true
layout: event
expiryDate: 2021-03-08T22:00:00+00:00
event:
  start: 2021-03-08T21:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 17
cover:
  image: tracks/azerbaijan/baku.png
---

Race 17 takes place at the Baku City Circuit: an unpredictable, 3.7 mile track around the streets of the Azerbaijan capital. 

20 turns for our drivers to navigate, including the infamous turn 8 - one of the tightest and most challenging corners of the season.