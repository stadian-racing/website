---
draft: false
title: Sochi Autodrome
tags: ["F1 2020", "Daily League", "Season 5"] 
keywords: ["racing","event", "f1 2020"]
author: Scanline
layout: event
expiryDate: 2021-02-17T22:00:00+00:00
event:
  start: 2021-02-17T21:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 3
cover:
  image: tracks/russia/sochi.png
---

Race 3 takes us to the Russian 5.8 kilometre Sochi Autodrom. It has two notable overtaking opportunities, into turn 1 and then turn 13. 18 corners in total here, 12 to the right and 6 to the left, and it's an average lap speed of around 209 kph