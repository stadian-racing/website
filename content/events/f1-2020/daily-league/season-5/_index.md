---
draft: false
title: Season 5
tags: ["F1 2020", "Daily League"] 
keywords: ["racing","event", "f1 2020"]
author: ["Scanline"]
expiryDate: 2021-04-02T21:00:00+00:00
---
Season 5 of the daily league will be running with F1 2020 cars starting February 15th!

<!-- more -->
We will have a Sprint race (5 laps - One-Shot Qualify) followed by a Feature Race (25% / full reverse grid)* 

This season we will be running Monday - Friday @ 21:00 GMT. The scope of the league is to promote fun relaxed entertainment for its members. The league is to promote casual fun without commitments. The overall goal is to provide an atmosphere where individuals can bond and enjoy this game and sport together.

### Settings
You can see all the race settings here: [Settings](./settings)

### Points
You can see how all the points are calculated here: [Points](./points)

> *dnf/dsq will be placed at the end

## Races

---
