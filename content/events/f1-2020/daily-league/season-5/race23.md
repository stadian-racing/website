---
draft: false
title: Suzuka International Racing Course (Normal)
tags: ["F1 2020", "Daily League", "Season 5"]  
keywords: ["racing","event", "f1 2020"]
author: Scanline
layout: event
expiryDate: 2021-03-16T22:00:00+00:00
event:
  start: 2021-03-16T21:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 23
cover:
  image: tracks/japan/suzuka.png
---

In race 23 we head to the incredible figure-of-eight Suzuka Circuit. 18 corners, 10 to the right and 8 to the left, over a distance of 5.8 kilometers. Average lap speeds around here are fairly quick; if it stays dry then expect somewhere in the region of  219kph.