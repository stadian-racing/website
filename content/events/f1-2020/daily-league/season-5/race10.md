---
draft: false
title: Hungaroring
tags: ["F1 2020", "Daily League", "Season 5"] 
keywords: ["racing","event", "f1 2020"]
author: Scanline
layout: event
expiryDate: 2021-02-26T22:00:00+00:00
event:
  start: 2021-02-26T21:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 10
cover:
  image: tracks/hungary/hungaroring.png
---

Race 10 takes us Northeast of Budapest for the next race, at the 4.3km Hungaroring circuit. 14 corners here - 8 to the right and 6 to the left - on a track where **downforce** is king, and passing is notoriously difficult.