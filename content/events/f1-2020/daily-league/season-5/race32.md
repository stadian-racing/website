---
draft: false
title: Autodromo Jose Carlos Pace
tags: ["F1 2020", "Daily League", "Season 5"]  
keywords: ["racing","event", "f1 2020"]
author: Scanline
layout: event
expiryDate: 2021-03-29T22:00:00+00:00
event:
  start: 2021-03-29T21:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 32
cover:
  image: tracks/brazil/interlagos.png
---

Race 32 will be held at the Interlagos in Brazil - a 4.3 kilometre circuit with 9 lefts and 6 rights, for a total of 15 corners. 

The fastest lap today should have an average speed of around 217 kph if, of course, the weather stays dry.
