---
draft: false
title: "Bahrain International Circuit (Normal)"
tags: ["F1 2020", "Daily League", "Season 4"]  
keywords: ["racing","event", "f1 2020"]
author: "Scanline"
hideSummary: true
expiryDate: 2021-02-06T20:00:00+00:00
layout: event
event:
  start: 2021-02-06T19:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 30
cover:
  image: tracks/bahrain/bahrain.png
---
