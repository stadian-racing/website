---
draft: false
title: "Bahrain International Circuit (Normal)"
tags: ["F1 2020", "Daily League", "Season 4"] 
keywords: ["racing","event", "f1 2020"]
author: "Scanline"
hideSummary: true
layout: event
expiryDate: 2021-01-11T22:00:00+00:00
event:
  start: 2021-01-11T21:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 7
cover:
  image: tracks/bahrain/bahrain.png
---

