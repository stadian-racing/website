---
draft: false
title: "Circuit de Barcelona-Catalunya"
tags: ["F1 2020", "Daily League", "Season 4"]  
keywords: ["racing","event", "f1 2020"]
author: "Scanline"
hideSummary: true
layout: event
expiryDate: 2021-01-23T20:00:00+00:00
event:
  start: 2021-01-23T19:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 18
cover:
  image: tracks/spain/circuit-de-catalunya.png
---
