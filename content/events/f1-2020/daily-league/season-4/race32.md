---
draft: false
title: "Circuit of the Americas (Short)"
tags: ["F1 2020", "Daily League", "Season 4"]  
keywords: ["racing","event", "f1 2020"]
author: "Scanline"
hideSummary: true
expiryDate: 2021-02-09T22:00:00+00:00
layout: event
event:
  start: 2021-02-09T21:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 32
cover:
  image: tracks/us/austin-short.png
---
