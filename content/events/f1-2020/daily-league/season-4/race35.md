---
draft: false
title: "Sochi Autodrom"
tags: ["F1 2020", "Daily League", "Season 4"]  
keywords: ["racing","event", "f1 2020"]
author: "Scanline"
expiryDate: 2021-02-12T22:00:00+00:00
hideSummary: true
layout: event
event:
  start: 2021-02-12T21:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 35
cover:
  image: tracks/russia/sochi.png
---
