---
draft: false
title: Season 4
tags: ["F1 2020", "Daily League"] 
keywords: ["racing","event", "f1 2020"]
author: ["Scanline"]
expiryDate: 2021-02-13T21:00:00+00:00
---

Season 4 of the daily league will be running with F1 2020 Multiplayer cars starting January 4th - February 10th. 

We will have a Feature Race (25% / One-Shot Qualify) followed by a Sprint race (5 laps - reverse grid based on results of the feature race). 
<!--more-->

This season we are changing it up a little bit. Every Saturday the Feature Race will be 100% / 18min Qualify and is now named **"FULL RACE SATURDAYS"** (There will not be a 5 lap sprint on Saturdays). 

The scope of the league is to promote fun relaxed entertainment for its members. The league is to promote casual fun without commitments. The overall goal is to provide an atmosphere where individuals can bond and enjoy this game and sport together.

For the points system we will share a spreadsheet that will store the position you received for each race.  

The feature race (25%) will receive points for the top ten drivers (25, 18, 15, 12, 10, 8, 6, 4, 2, 1).  

The sprint race (5 Laps) will receive points for the top eight finishers (15, 12, 10, 8, 6, 4, 2, 1).  Both races will receive 1 point for the fastest lap, please note that you must finish the race to receive points. 

## Races

---
