---
draft: false
title: Silverstone
tags: ["F1 2020", "Daily League", "Season 6"]  
keywords: ["racing","event", "f1 2020"]
author: Scanline
stadia: Scanline#6792
description: F1 cars
expiryDate: 2021-05-18T20:00:00+00:00
layout: event
event:
  start: 2021-05-18T20:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 30
cover:
  image: tracks/uk/silverstone.png
---
Race 30 takes us Northeast of Budapest for the next race, at the 4.3km Hungaroring circuit. 

14 corners here - 8 to the right and 6 to the left - on a track where **downforce** is king, and passing is notoriously difficult.