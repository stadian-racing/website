---
draft: false
title: Bahrain
tags: ["F1 2020", "Daily League", "Season 6"] 
keywords: ["racing","event", "f1 2020"]
author: Scanline
stadia: Scanline#6792
layout: event
description: F2 cars
expiryDate: 2021-04-19T20:00:00+00:00
event:
  start: 2021-04-19T20:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 9
cover:
  image: tracks/bahrain/bahrain.png
---
Today we will be racing around 5.4 kilometres of the magnificent Bahrain International Circuit, with 15 corners and 2 good passing opportunities into turns 1 and 4. Keep an eye out for drivers locking the front left tyre into the tricky braking zone of turn 10. 

