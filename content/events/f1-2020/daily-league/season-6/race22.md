---
draft: false
title: Shanghai
tags: ["F1 2020", "Daily League", "Season 6"]  
keywords: ["racing","event", "f1 2020"]
author: Scanline
stadia: Scanline#6792
layout: event
description: F1 cars
expiryDate: 2021-05-06T20:00:00+00:00
event:
  start: 2021-05-06T20:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 21
cover:
  image: tracks/china/shanghai.png
---
Today we'll be in the Yangtze river delta home of the 16 corners that make up the Shanghai International Circuit. 54% of this 5.3 kilometre lap is taken at full throttle, and we'll be getting up to speeds of around 322 kph with DRS assistance down the back straight before they brake into the tight hairpin at turn 14.
