---
draft: false
title: Points
tags: ["F1 2020", "Daily League", "Season 6"] 
keywords: ["racing","event", "f1 2020"]
author: Scanline
stadia: Scanline#6792
---

For the points system we will share a spreadsheet that will store the position you received for each race.

[Points Sheet](https://tiny.cc/src_f1)

Both races will receive **1 (one)** point for the fastest lap

> *You must finish the race to receive any points.*

### Feature race (25%):

1st
: 25

2nd
: 18

3rd
: 15

4th
: 12

5th
: 10

6th
: 8

7th
: 6

8th
: 4

9th
: 2

10th
: 1

---

### Sprint Race (5 laps):

1st
: 15

2nd
: 12

3rd
: 10

4th
: 8

5th
: 6

6th
: 4

7th
: 2

8th
: 1
