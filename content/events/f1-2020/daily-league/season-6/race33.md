---
draft: false
title: Yas Marina
tags: ["F1 2020", "Daily League", "Season 6"]  
keywords: ["racing","event", "f1 2020"]
author: Scanline
stadia: Scanline#6792
layout: event
description: F2 cars
expiryDate: 2021-05-21T20:00:00+00:00
event:
  start: 2021-05-21T20:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 33
cover:
  image: tracks/abu-dhabi/yas-marina.png
---

Today takes place in Abu Dhabi. We'll have 21 corners at the Yas Island circuit with 10 to the right and 11 to the left. It's a total lap distance of 5.4 kilometres. 2 long straights open up some passing opportunities into the chicanes, and we expect average lap speeds of around 198 kph.