---
draft: false
title: Suzuka
tags: ["F1 2020", "Daily League", "Season 6"]  
keywords: ["racing","event", "f1 2020"]
author: Scanline
stadia: Scanline#6792
layout: event
description: F1 cars
expiryDate: 2021-05-04T20:00:00+00:00
event:
  start: 2021-05-04T20:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 20
cover:
  image: tracks/japan/suzuka.png
---

Today we head to the incredible figure-of-eight Suzuka Circuit. 18 corners, 10 to the right and 8 to the left, over a distance of 5.8 kilometers. Average lap speeds around here are fairly quick; if it stays dry then expect somewhere in the region of  219kph.