---
draft: false
title: Season 6
tags: ["F1 2020", "Daily League"] 
keywords: ["racing","event", "f1 2020", "f2 2020"]
author: "Scanline"
stadia: Scanline#6792
description: F1 & F2 Cars
expiryDate: 2021-05-26T21:00:00+00:00
---
Season 6 of the Daily League will be running with F1 & F2 2020 cars starting  Wednesday April 7th!

We will be alternating each day between F2 2020 and F1 2020 Cars!.
<!-- more -->

We will follow the F2 format for both f2/f1 races. 

Each race will consist of a Feature Race (25% / One-Shot Qualify with formation lap) followed by a Sprint race (5 laps  / full reverse grid, ** dnf/dsq will be placed at the rear).

The event will run Monday - Friday @ 22:00 CET. 

The scope of the league is to promote fun relaxed entertainment for its members. The league is to promote casual fun without commitments. The overall goal is to provide an atmosphere where individuals can bond and enjoy this game and sport together.  

### Settings
You can see all the race settings here: [Settings](./settings)

### Points
You can see how all the points are calculated here: [Points](./points)

> *dnf/dsq will be placed at the end

## Races

--- 