---
draft: false
title: Interlagos
tags: ["F1 2020", "Daily League", "Season 6"]  
keywords: ["racing","event", "f1 2020"]
author: Scanline
stadia: Scanline#6792
layout: event
description: F1 cars
expiryDate: 2021-05-14T20:00:00+00:00
event:
  start: 2021-05-14T20:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 28
cover:
  image: tracks/brazil/interlagos.png
---

Today will be held at the Interlagos in Brazil - a 4.3 kilometre circuit with 9 lefts and 6 rights, for a total of 15 corners. The fastest lap today should have an average speed of around 217 kph if, of course, the weather stays dry.
