---
draft: false
title: Sochi
tags: ["F1 2020", "Daily League", "Season 6"]  
keywords: ["racing","event", "f1 2020"]
author: Scanline
stadia: Scanline#6792
layout: event
description: F2 Cars
expiryDate: 2021-04-23T20:00:00+00:00
event:
  start: 2021-04-23T20:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 13
cover:
  image: tracks/russia/sochi.png
---
Today takes us to the Russian 5.8 kilometre Sochi Autodrom. It has two notable overtaking opportunities, into turn 1 and then turn 13. 18 corners in total here, 12 to the right and 6 to the left, and it's an average lap speed of around 209 kph
