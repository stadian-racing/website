---
draft: false
title: Circuit de Spa-Francorchamps
tags: ["F1 2020", "Daily League", "Season 6"]  
keywords: ["racing","event", "f1 2020"]
author: Scanline
stadia: Scanline#6792
layout: event
description: F2 cars
expiryDate: 2021-05-25T20:00:00+00:00
event:
  start: 2021-05-25T20:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 35
cover:
  image: tracks/belgium/spa.png
---

Today takes us to the Circuit de Spa-Francorchamps in Belgium, this historic venue is a 19 corner circuit with a lap distance of 7 kilometres. There's over 100 metres of elevation change here, and with long stretches of the lap spent flat out, a good top speed will be vital for success.