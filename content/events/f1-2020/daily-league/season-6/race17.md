---
draft: false
title: Circuit Gilles Villeneuve
tags: ["F1 2020", "Daily League", "Season 6"] 
keywords: ["racing","event", "f1 2020"]
author: Scanline
stadia: Scanline#6792
layout: event
expiryDate: 2021-04-29T20:00:00+00:00
description: F2 cars
event:
  start: 2021-04-29T20:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 17
cover:
  image: tracks/canada/gilles-villeneuve.png
---

Today will be held at the Circuit Gilles-Villeneuve in Canada, with around 60% of this 4.3 kilometre circuit taken at full throttle.  There are 14 corners to navigate, the very last of which has its very own infamous history, and could be the cause of a wreck today! 
