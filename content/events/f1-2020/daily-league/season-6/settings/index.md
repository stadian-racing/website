---
draft: false
title: Settings
tags: ["F1 2020", "Daily League", "Season 6"] 
keywords: ["racing","event", "f1 2020"]
author: Scanline
stadia: Scanline#6792
---
### Feature Race
{{< image src="feature-race5.png" >}}

### Sprint Race
{{< image src="sprint5.png" >}}

### Race Settings
{{< image src="settings2.png" >}}
### Assists
{{< image src="settings3.png" >}}