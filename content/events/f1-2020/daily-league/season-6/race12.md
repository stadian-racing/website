---
draft: false
title: Silverstone
tags: ["F1 2020", "Daily League", "Season 6"]  
keywords: ["racing","event", "f1 2020"]
author: Scanline
stadia: Scanline#6792
layout: event
description: F1 Cars
expiryDate: 2021-04-22T20:00:00+00:00
event:
  start: 2021-04-22T20:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 12
cover:
  image: tracks/uk/silverstone.png
---

We will be at the 5.8 kilometre long Silverstone Circuit, this event is one of the longest of the season, with 18 corners in the current layout. 

With average lap speeds reaching around 233 kph, it's also one of the quickest tracks in the sport; watch for cars taking the right-handers of the Abbey (Turn 1) and Copse (Turn 9) flat out.
