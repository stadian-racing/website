---
draft: false
title: Hanoi
tags: ["F1 2020", "Daily League", "Season 6"] 
keywords: ["racing","event", "f1 2020"]
author: Scanline
stadia: Scanline#6792
layout: event
expiryDate: 2021-04-20T20:00:00+00:00
description: F1 cars
event:
  start: 2021-04-20T20:00:00+00:00
  duration: 1h
  show_calendar_link: true
weight: 10
cover:
  image: tracks/vietnam/fia/hanoi.jpg
---

Today will be held at the Hanoi Track in Vietnam.  

It's a track that combines the bespoke design of a traditional race track with the tight, close barriers of a street circuit.  
23 corners and a total distance of 5.5 kilometres. Watch out in particular for overtakes into the braking zone at turn 11