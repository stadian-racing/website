---
title: "Honda NSX vs Nissan GTR: Face Off"
author: KRAY
tags: ["GRID"]
keywords: ["racing","event", "grid"]
description: ""
expiryDate: 2021-01-20T21:00:00+00:00
layout: event
event:
    start: 2021-01-20T21:15:00+00:00
    duration: 2h
    show_calendar_link: true
cover:
    image: "grid-face-off.png"
    alt: "GRID game poster"
---

Format
: Team/Race

Hot Lap Qualifying
: Yes

Return to Skirmish
: No

Vehicle Damage
: Full

Terminal Damage
: Yes

---

## Race Calendar

The tracks we will use are a mix of short and long with different time of day and weather. Each track should take about 10 minutes to do.

Stage 1
: Sydney, Druid Circuit Reversed, Sunset, 11 Laps

Stage 2
: Suzuka, Int Racing Course, Dusk, 7 Laps

Stage 3
: Brands Hatch, GP Circuit, Day Rain, 7 Laps

Stage 4
: Red Bull Ring, GP Circuit, Overcast, 7 Laps

Stage 5
: Crescent Valley, National Circuit, Night, 11 Laps

---

## Points & Leaderboard
In-game points system will be used and calculated at the end of each round presented on a tracked leaderboard.

There will be 1 additional point awarded in each race for:
 - Qualifying Pole
 - Fastest Lap
 - Podium

There will also be a constructors leaderboard based on the in-game points system

The tracks we will use are a mix of short and long with different time of day and weather. Each track should take about 10 minutes to do.

See you on the track!
