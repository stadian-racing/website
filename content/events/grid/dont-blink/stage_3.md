---
title: TC-1 Specials Porsche 935/78
author: Redfox
tags: ["GRID", "Don't Blink!"]
expiryDate: 2021-09-29T22:00:00+00:00
layout: event
event:
  start: 2021-09-29T20:00:00+00:00
  duration: 60m
  show_calendar_link: true
weight: 3
---
## Vehicle 

**TC-1 Specials Porsche 935/78**

---

## Races

SS GP 2009
: Day, 10 Laps

SS GP Rev
: Afternoon, 8 Laps

SEP GP Reversed
: Night, 8 Laps
---

## General Info

Hot Lap Qualifying
: No

Return to Skirmish
: No

Vehicle Damage
: No

Terminal Damage
: No

Grid
: Reversed


> Reversed Grids to allow for lots of overtaking fun. Lots of laps, so no need to rush it in the early race. Enjoy the drive!