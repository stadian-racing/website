---
title: G7 McLaren M8D
author: Redfox
tags: ["GRID", "Don't Blink!"]
expiryDate: 2021-09-15T22:00:00+00:00
layout: event
event:
  start: 2021-09-15T20:00:00+00:00
  duration: 60m
  show_calendar_link: true
weight: 1
---
## Vehicle 

**G7 McLaren M8D**

---

## Races

IND North
: Day, 13 Laps

IND Oval
: Afternoon, 22 Laps

CV Oval
: Night, 23 Laps
---

## General Info

Hot Lap Qualifying
: No

Return to Skirmish
: No

Vehicle Damage
: No

Terminal Damage
: No

Grid
: Reversed


> Reversed Grids to allow for lots of overtaking fun. Lots of laps, so no need to rush it in the early race. Enjoy the drive!