---
title: Renault R26
author: Redfox
tags: ["GRID", "Don't Blink!"]
expiryDate: 2021-10-06T22:00:00+00:00
layout: event
event:
  start: 2021-10-06T20:00:00+00:00
  duration: 60m
  show_calendar_link: true
weight: 4
---
## Vehicle 

**Renault R26**

---

## Races

SEP GP
: Day, 9 Laps

SUZ GP Rev
: Dusk, 9 Laps

SUZ GP
: Night, 9 Laps
---

## General Info

Hot Lap Qualifying
: No

Return to Skirmish
: No

Vehicle Damage
: No

Terminal Damage
: No

Grid
: Reversed


> Reversed Grids to allow for lots of overtaking fun. Lots of laps, so no need to rush it in the early race. Enjoy the drive!