---
title: WTA Honda CRX
author: Redfox
tags: ["GRID", "Don't Blink!"]
expiryDate: 2021-09-22T22:00:00+00:00
layout: event
event:
  start: 2021-09-22T20:00:00+00:00
  duration: 60m
  show_calendar_link: true
weight: 2
---
## Vehicle 

**WTA Honda CRX**

---

## Races

CV GP
: Overcast, 9 Laps

OKU Grand
: Sunset, 9 Laps

OKU Grand Reversed
: Dusk, 9 Laps
---

## General Info

Hot Lap Qualifying
: No

Return to Skirmish
: No

Vehicle Damage
: No

Terminal Damage
: No

Grid
: Reversed


> Reversed Grids to allow for lots of overtaking fun. Lots of laps, so no need to rush it in the early race. Enjoy the drive!