---
title: Don't Blink!
author: Redfox
tags: ["GRID"]
keywords: ["racing","event", "grid"]
description: "A themed series of individual race events, showcasing cars from some of the fastest car classes.  The top speeds obtained by these cars, will have you moving a bajillion feet per second; so Don’t Blink!"
expiryDate: 2021-10-07T22:00:00+00:00
cover:
  image: dont_blink.png
---

A themed series of individual race events, showcasing cars from some of the fastest car classes.  The top speeds obtained by these cars, will have you moving a bajillion feet per second; so Don’t Blink!

Each race day will have a specified car for us to explore and compete with.  These untamed beasts will surely provide chaotic moments; we will need to do our best to keep them under control.
<!--more-->

Sticking with the format of our last couple series. 3 races per event, about an hour long, reversed grids etc.

Weekly individual events. No series championship points.  Drop in for one raceday, or come play all 4 events as you wish.

Up to 16 drivers.

Drivers will be asked to confirm their intent in [discord](/discord) to race each week, by Tuesday afternoon, for races on the following Wednesday evening, in order to give hosts time to prepare team rosters.

---
### Format

Each race day will feature a specified car.  2 locations, for 3 races.

No Qualifying.  Reversed Grids.

Each race will be 15 minutes each; lots of time to show off your clean driving abilities, as the field sorts itself each race.

---

## Stages
