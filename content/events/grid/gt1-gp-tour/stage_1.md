---
title: Stage 1 | Ferrari 448
author: Redfox
stadia: Redfox#2355
tags: ["GRID", "GT-1 GP"]
keywords: ["racing","event", "grid"]
expiryDate: 2021-06-09T21:00:00+00:00
layout: event
event:
  start: 2021-06-09T20:00:00+00:00
  duration: 90m
  show_calendar_link: true
cover:
  image: events/grid/gt1-gp-tour/GT1S1.png
---
## Circuits

BH GP
: 11 Laps 

Conditions
: Day
----
BH GP Rev
: 11 Laps

Conditions
: Afternoon
----
SS GP Rev
: 8 Laps

Conditions
: Night
