---
title: Stage 4 | Chevrolet Corvette 
author: Redfox
stadia: Redfox#2355
tags: ["GRID", "GT-1 GP"]
keywords: ["racing","event", "grid"]
expiryDate: 2021-06-30T21:00:00+00:00
layout: event
event:
  start: 2021-06-30T20:00:00+00:00
  duration: 90m
  show_calendar_link: true
cover:
  image: events/grid/gt1-gp-tour/GT1S4.jpg
---
## Circuits

SUZ GP
: 7 Laps 

Conditions
: Day
----
CV Infield
: 11 Laps

Conditions
: Dusk
----
CV GP
: 8 Laps

Conditions
: Night
