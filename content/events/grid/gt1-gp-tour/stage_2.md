---
title: Stage 2 | Porsche 911
author: Redfox
stadia: Redfox#2355
tags: ["GRID", "GT-1 GP"]
keywords: ["racing","event", "grid"]
expiryDate: 2021-06-16T21:00:00+00:00
layout: event
event:
  start: 2021-06-16T20:00:00+00:00
  duration: 90m
  show_calendar_link: true
cover:
  image: events/grid/gt1-gp-tour/GT1S2.png
---
## Circuits

SS GP
: 8 Laps 

Conditions
: Overcast
----
SEP GP Rev
: 7 Laps

Conditions
: Sunset
----
SEP GP Rev
: 7 Laps

Conditions
: Dusk
