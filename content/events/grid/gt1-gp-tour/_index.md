---
title: GT-1 Grand Prix Tour
author: Redfox
stadia: Redfox#2355
tags: ["GRID"]
description: Tour the world’s fast and famous GP circuits, in each of the GT-1 cars
keywords: ["racing","event", "grid"]
expiryDate: 2021-07-22T22:00:00+00:00
cover:
  image: events/grid/gt1-gp-tour/GT1PM.png
---
Tour the world’s fast and famous GP circuits, in each of the GT-1 cars!!!
<!--more-->

Drivers will be asked to confirm their intent in [discord](/discord) to race each week, by Tuesday afternoon, for races on the following Wednesday evening, in order to give hosts time to prepare.

---

## General Info
Cars
: GT Group 1

Hot Lap Qualifying
: No - Reverse Grid

Return to Skirmish
: No

Vehicle Damage
: No

Terminal Damage
: No

## Format
Each race day will feature 
 - A GT-1 Car.  
 - 2 locations, for 3 races.
 - No Qualifying.  
 - Reversed Grids.

Each race will be 13-15 minutes each; lots of time to show off your clean driving abilities, as the field sorts itself each race.

---