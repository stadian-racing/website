---
title: Stage 5 | Ford GT
author: Redfox
stadia: Redfox#2355
tags: ["GRID", "GT-1 GP"]
keywords: ["racing","event", "grid"]
expiryDate: 2021-07-14T21:00:00+00:00
layout: event
event:
  start: 2021-07-14T20:00:00+00:00
  duration: 90m
  show_calendar_link: true
cover:
  image: events/grid/gt1-gp-tour/GT1S5.png
---
## Circuits

SYD Gardner GP Rev
: 10 Laps

Conditions
: Dusk
----
SYD Brabham
: 8 Laps

Conditions
: Night
----
IND Road
: 11 Laps

Conditions
: Day
