---
title: Stage 6 | Dodge Viper 
author: Redfox
stadia: Redfox#2355
tags: ["GRID", "GT-1 GP"]
keywords: ["racing","event", "grid"]
expiryDate: 2021-07-21T21:00:00+00:00
layout: event
event:
  start: 2021-07-21T20:00:00+00:00
  duration: 90m
  show_calendar_link: true
cover:
  image: events/grid/gt1-gp-tour/GT1S6.png
---
## Circuits

IND Sport
: 10 Laps

Conditions
: Day
----
RBR GP Rev
: 10 Laps

Conditions
: Overcast
----
RBR GP
: 10 Laps

Conditions
: Afternoon
