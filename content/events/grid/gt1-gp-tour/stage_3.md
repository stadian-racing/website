---
title: Stage 3 | Aston Martin Vantage
author: Redfox
stadia: Redfox#2355
tags: ["GRID", "GT-1 GP"]
keywords: ["racing","event", "grid"]
expiryDate: 2021-06-23T21:00:00+00:00
layout: event
event:
  start: 2021-06-23T20:00:00+00:00
  duration: 90m
  show_calendar_link: true
cover:
  image: events/grid/gt1-gp-tour/GT1S3.png
---
## Circuits

OKU GP
: 8 Laps 

Conditions
: Day
----
OKU GP Rev
: 8 Laps

Conditions
: Afternoon
----
SUZ GP Rev
: 7 Laps

Conditions
: Night
