---
title: VW Golf Sundowner
author: ELPorto98
stadia: ELPorto98#6199
tags: ["GRID"]
keywords: ["racing","event", "grid"]
description: "TC2 - Volkswagen Golf GTI TCR"
layout: event
expiryDate: 2021-07-07T22:00:00+00:00
cover:
    image: "src-sundown.jpg"
    alt: "Sundowner poster"
event:
    start: 2021-07-07T20:00:00+00:00
    duration: 90m
    show_calendar_link: true
---
Enjoy some casual racing fun with the famous VW Golf GTI on beautifully illuminated European tracks, cities and landscapes during sunset. 

For this event you will be hosted by ELPorto98, so please make sure to get on his Stadia friendlist in order to ease up inviting procedures for him. Also please make sure to update your sign ups properly and in time up to a few hours before the event starts. Thanks a lot for your help!
<!--more-->
Not much else to say here. Looking forward to meet many of you on track. Please keep in mind - drinking and driving is appreciated for this event! Cheers.:tropical_drink:

Drivers will be asked to confirm their intent in [discord](/discord) to race each week, by Tuesday afternoon, for races on the following Wednesday evening, in order to give hosts time to prepare team rosters.

---
### Car
TC2 - Volkswagen Golf GTI TCR

---

## General Info

Hot Lap Qualifying
: No

Return to Skirmish
: No

Vehicle Damage
: Yes

Terminal Damage
: No

---

## Grid Orders
Race 1
: Driver level reversed

Races 2 - 5
: Previous race results reversed

---

## Tracks
Barcelona
: Torres Venecianes Loop (20 laps)

Brands Hatch
: Indy Circuit (15 laps)

Paris
: Champs-Élysées (7 laps)

Red Bull Ring
: South Circuit Reversed (15 laps)

Silverstone
: International Circuit Reversed (10 laps)

### Conditions
All races will be held during sunset (dry)


