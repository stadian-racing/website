---
title: "Honda NSX vs Nissan GTR: The Rivals"
author: KRAY
tags: ["GRID"]
keywords: ["racing","event", "grid"]
description: "Welcome to the Honda NSX vs Nissan GTR, event where we will race in 2 teams across 2 divisions on 5 stages per division with up to 32 drivers."
expiryDate: 2021-01-16T21:00:00+00:00
layout: event
event:
    start: 2021-01-15T21:15:00+00:00
    duration: 2h
    show_calendar_link: true
cover:
    image: "TRHVN.jpg"
    alt: "GRID game poster"
---

Format
: Team/Race

Players
: Up to 32

Divisions
: 2

Hot Lap Qualifying
: Yes

Return to Skirmish
: No

Vehicle Damage
: Visual

Terminal Damage
: No

---

## Race Calendar
15th January 2021

Stage 1
: Sepang Int Circuit, Full Track, 9 Laps, Day

Stage 2
: Silverstone Circuit, GP, 9 Laps, Overcast

Stage 3
: Indianapolis, Sprint Circuit, 9 Laps, Day Rain

Stage 4
: Okutama GP, Grand Circuit, 9 Laps, Dusk

Stage 5
: Crescent Valley, GP Circuit, 9 Laps, Night Rain

---

## Points & Leaderboard
In-game points system will be used and calculated at the end of each round presented on a tracked leaderboard.

There will be 1 additional point awarded in each race for:
 - Qualifying Pole
 - Fastest Lap


If you would like to take part in this event, please register your interest in the {{< ext_link text="Discord" href="https://discord.com/channels/693246888203517982/796182319915597845/796445075613810724" >}}

If both teams are full, you can react to the Reserve List to be considered if we have any drop outs.

See you on the track!
