---
draft: false
title: Ferrari Timeline Championship
author: KRAY
keywords: ["racing","event", "grid"]
tags: ["GRID"]
expiryDate: 2021-05-10T21:00:00+00:00
weight: 2
cover:
    image: "FTCMP.jpg"
    alt: "GRID game poster"
---

Hot Lap Qualifying:
: Yes

Return to Skirmish:
: Yes

Vehicle Damage:
: Full

Terminal Damage:
: Yes

<!--more-->
-----

> Terminal Damage is turned on in an effort to promote clean racing expected in SRC events, basic rules and etiquette can be viewed here: [rules](/rocr)*

----

This is a Ferrari only event, where we will race all 8 cars the game has to offer across 24 races in 8 rounds (3 races per round) starting with the 1967 Historic and right through the years finishing with the modern 2017 Hypercar.

Players will need to own every Ferrari in the game and also have the “Ultimate Edition” expansion for the Season 3 DLC content to take part in this event.
> *DLC content includes the Track Day Hypercars class which gives you access to the Ferrari FXX-K Evo.

## Race Calendar
All races will be a mix of weather conditions and time of day. (see individual stages for info) Each round takes places at the same time on every second Sunday of the month.

----
