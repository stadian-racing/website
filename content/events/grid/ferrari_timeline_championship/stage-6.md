---
title: Stage 6
author: KRAY
expiryDate: 2021-04-11T23:50:00+00:00
layout: event
tags: ["GRID","Ferrari Timeline"]
event:
  start: 2021-04-11T21:15:00+00:00
  duration: 1h
  show_calendar_link: true
cover:
  image: cars/ferrari/2012-599xx-evo.jpg
  alt: 2012 Ferrari 599xx Evo
description: 2012 Ferrari 599xx Evo
---
Track
: Barcelona - High Street

Laps
: TBD

Conditions
: TBD

----
Track
: Havana - Castillo View 

Laps
: TBD

Conditions
: TBD

----
Track
: Shanghai - Napu Bridge Circuit 

Laps
: TBD

Conditions
: TBD

----
Track
: Okutama Sprint - Sakura Pass

Laps
: 1

Conditions
: TBD