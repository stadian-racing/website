---
title: Stage 4
author: KRAY
expiryDate: 2021-03-14T23:50:00+00:00
layout: event
tags: ["GRID","Ferrari Timeline"]
event:
  start: 2021-03-14T21:15:00+00:00
  duration: 1h
  show_calendar_link: true
cover:
  image: cars/ferrari/2004-f430.jpg
  alt: 2004 Ferrari F430
description: 2004 Ferrari F430
---
Track
: Barcelona - Memorial Run

Laps
: 7

Conditions
: Day

----
Track
: Havana - Paseo de Marti

Laps
: 6

Conditions
: Sunset

----
Track
: San Francisco - Grand Prix Circuit B

Laps
: 7

Conditions
: Dusk

----
Track
: Okutama Sprint - Tatsu Valley

Laps
: 1

Conditions
: Night