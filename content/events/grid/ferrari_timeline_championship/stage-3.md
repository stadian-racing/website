---
title: Stage 3
author: KRAY
expiryDate: 2021-02-28T23:50:00+00:00
layout: event
tags: ["GRID","Ferrari Timeline"]
event:
  start: 2021-02-28T21:15:00+00:00
  duration: 1h
  show_calendar_link: true
cover:
  image: cars/ferrari/1978-512-bb-lm.jpg
  alt: 1978 Ferrari 512 BB LM
description: 1978 Ferrari 512 BB LM
---
Track
: Paris - Avenue De New-York

Laps
: 6

Conditions
: Day Rain

----
Track
: Paris - Circuit De La Seine

Laps
: 7

Conditions
: Sunset Rain

----
Track
: Paris - Champs Élysées

Laps
: 8

Conditions
: Night Rain