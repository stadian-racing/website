---
title: Stage 1
author: Redfox
stadia: Redfox#2355
description: Super Tourers
expiryDate: 2021-03-03T22:00:00+00:00
layout: event
event:
  start: 2021-03-03T21:00:00+00:00
  duration: 2h
  show_calendar_link: true
---
## Vehicle 

**Ford Falcon FG-X**

## Races

CV Club
: Day, 12 Laps

IND Sport
: Day, 7 Laps

SYD Brabham
: Afternoon, 6 Laps

SYD Druitt
: Afternoon, 11 Laps

