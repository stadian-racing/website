---
title: Stage 3
author: Redfox
stadia: Redfox#2355
description: Pro Trucks
expiryDate: 2021-03-17T22:00:00+00:00
layout: event
event:
  start: 2021-03-17T21:00:00+00:00
  duration: 2h
  show_calendar_link: true
---
## Vehicle 

**Dumont Type 37**

## Races

IND North
: Day, 8 Laps

IND Sport Rev
: Day, 7 Laps

CV National
: Afternoon, 10 Laps

CV Infield
: Afternoon, 7 Laps
