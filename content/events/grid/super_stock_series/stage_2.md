---
title: Stage 2
author: Redfox
stadia: Redfox#2355
description: Pro Trucks
expiryDate: 2021-03-10T22:00:00+00:00
layout: event
event:
  start: 2021-03-10T21:00:00+00:00
  duration: 2h
  show_calendar_link: true
---
## Vehicle 

**Dumont Type 37**

## Races

BAR Marina
: Day, 7 Laps

SHG Napu
: Day, 6 Laps

BH GP
: Afternoon, 7 Laps

SYD Garner Rev
: Afternoon, 6 Laps