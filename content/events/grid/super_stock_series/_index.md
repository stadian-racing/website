---
title: Super Stock Series
author: Redfox
stadia: Redfox#2355
tags: ["GRID"]
keywords: ["racing","event", "grid"]
description: ""
expiryDate: 2021-04-01T22:00:00+00:00
---
Super Tourers, Pro Trucks, and Oval Stocks.

Some of these races will find the car of choice in their natural habitat, on familiar courses, and other times, they may seem a long way from home where you’d least expect them.
<!--more-->

Our final event will be on the Ovals.  Will be fun slipstream racing.

Sydney, Crescent Valley, and Indianapolis will play our primary hosts, with a few other special stops around the world along the way.

Drivers will be asked to confirm their intent in [discord](/discord) to race each week, by Tuesday afternoon, for races on the following Wednesday evening, in order to give hosts time to prepare.

---

## General Info

Hot Lap Qualifying
: Yes

Return to Skirmish
: No

Vehicle Damage
: No

Terminal Damage
: No

---

## Points

In-game points system will be used.

There will be 1 additional point awarded in each race for: Qualifying Pole & Fastest Lap

---