---
title: City Heights
author: Kray
stadia: KRAY#2432
tags: ["GRID"]
keywords: ["racing","event", "grid"]
description: "Ford Mustang Mach 1"
expiryDate: 2021-05-23T19:30:00+00:00
layout: event
event:
  start: 2021-05-23T19:30:00+00:00
  duration: 90m
  show_calendar_link: true
cover:
  image: "mach1.jpg"
  alt: "City Heights Event Poster"
---
Welcome to San Francisco! :city_sunset:

Following on from “Havana Nights”, I can now announce an add-on event for this mini-series.
“City Heights” set in sunny San Francisco where we will race in the Mustang Mach 1.

<!--more-->

Drivers will be asked to confirm their intent in [discord](/discord) to race, in order to give hosts time to prepare.

Invites will be sent at 21:15 CEST and we will start exactly at 21:30. 

See you on track! :v:

---
## Car
Ford Mustang Mach 1

---

## General Info

Hot Lap Qualifying
: Main races only

Return to Skirmish
: Between Sessions

Vehicle Damage
: Full

Terminal Damage
: Yes

> TD is turned on in an effort to promote clean racing expected in SRC events. Rules & etiquette can be viewed here: https://stadianracing.com/rocr

---
# Sessions
## Session 1
**City Heights Sprint Cup**

- Random Grid Start, Sprint Circuit (Sunset) 5 Laps
- Reverse Grid Start by Event Standings, Sprint Circuit B (Sunset Rain) 5 Laps

## Session 2
**City Heights Time Trial Trophy**

time trial mode (10 mins each)

- Random Rolling Start, Short Circuit (Sunset)
- Random Rolling Start, Short Circuit B (Sunset Rain)

## Session 3
**City Heights Main Race**

- Hot Lap Qualifying, Grand Prix Circuit (Sunset) 8 Laps
- Reverse Grid Start by Event Standings , Grand Prix Circuit B (Sunset Rain) 8 Laps