---
title: Stage 3
author: B1GDAVE
stadia: B1GDAVE#7099
tags: ["GRID", "Scooby Sundays"]
keywords: ["racing","event", "grid"]
expiryDate: 2021-06-20T21:00:00+00:00
layout: event
event:
  start: 2021-06-20T19:30:00+00:00
  duration: 90m
  show_calendar_link: true
cover:
  image: events/grid/scooby_sunday/SS0.3.jpg
---

## Grand Prix Circuits

Indianapolis: North circuit
: 8 Laps

Okutama GP: Grand
: 7 Laps

---
**Back to Skirmish**

---
## City Circuits

Paris: Avenue De New-York
: 6 laps

Havana: Revolucion Way
: 12 laps