---
title: Stage 4 (Bonus)
author: B1GDAVE
stadia: B1GDAVE#7099
tags: ["GRID", "Scooby Sundays"]
keywords: ["racing","event", "grid"]
expiryDate: 2021-06-27T21:00:00+00:00
layout: event
event:
  start: 2021-06-27T19:30:00+00:00
  duration: 90m
  show_calendar_link: true
cover:
  image: events/grid/scooby_sunday/SS0.4.jpg
---
Stage 4 is a bonus round where the 1st race of both GP and City will be random grid order with reverse standings for the final race.

## Weather
*possible rain on any track*

Grand Prix
: Day

City
: Night

## Grand Prix Circuits

Suzuka International
: 6 Laps

Sepang International: Full
: 6 Laps

---
**Back to Skirmish**

---
## City Circuits

Shanghai: Nanpu Bridge Circuit
: 6 laps

Barcelona: Fountain Loop
: 12 laps