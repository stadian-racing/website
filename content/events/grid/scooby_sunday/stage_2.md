---
title: Stage 2
author: B1GDAVE
stadia: B1GDAVE#7099
tags: ["GRID", "Scooby Sundays"]
keywords: ["racing","event", "grid"]
expiryDate: 2021-06-13T21:00:00+00:00
layout: event
event:
  start: 2021-06-13T19:30:00+00:00
  duration: 90m
  show_calendar_link: true
cover:
  image: events/grid/scooby_sunday/SS0.2.jpg
---

## Grand Prix Circuits

Brands Hatch
: 8 Laps

Cresent Valley
: 6 Laps

---
**Back to Skirmish**

---
## City Circuits

Havana: Castillio View
: 8 laps

San Francisco: Short Circuit B
: 12 laps