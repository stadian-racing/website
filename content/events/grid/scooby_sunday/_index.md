---
title: Scooby Sundays
author: B1GDAVE
stadia: B1GDAVE#7099
tags: ["GRID"]
keywords: ["racing","event", "grid"]
description: "Weekly Tournamant running over 4 stages (stage 4 being a bonus stage)"
expiryDate: 2021-06-28T19:30:00+00:00
cover:
  image: "ScoobySundayimage1.png"
  alt: "Scooby Sundays Event Poster"
---
Required Car
: Subaru Impreza WRX Tomei Cusco

---

Lets kick off June with a BANG!

I've been away from the SRC for a few months and have now returned, i had a few ideas for Tournaments before i left and one of them is now upon us. 

The idea came to me as i love this car both in real life and in the GRID game, It's a fast car yet can be controlled quite well with some practice so for any newcomers it can let them experience high speed racing while staying on the track HaHa!!

The car of choice is the Subaru Impreza WRX Tomei Cusco

The tournament is over 4 Stages with a small twist at the end.

<!--more-->

Drivers will be asked to confirm their intent to race in [discord](/discord), in order to give hosts time to prepare.

Invites will be sent at 19:15 UTC and we will start exactly at 19:30. 

See you on track! :v:

---

## General Info

Hot Lap Qualifying
: On (excluding Stage 4)

Return to Skirmish
: Between GP and City tracks to allow for set-up changes

Vehicle Damage
: Full

Terminal Damage
: Yes

Points System
: In-game points system used with additional points for Attendance,fastest lap,Pole & Podium

> TD is turned on in an effort to promote clean racing expected in SRC events. Rules & etiquette can be viewed here: https://stadianracing.com/rocr

---
# Sessions