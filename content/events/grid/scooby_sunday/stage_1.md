---
title: Stage 1
author: B1GDAVE
stadia: B1GDAVE#7099
tags: ["GRID", "Scooby Sundays"]
keywords: ["racing","event", "grid"]
expiryDate: 2021-06-06T21:00:00+00:00
layout: event
event:
  start: 2021-06-06T19:30:00+00:00
  duration: 90m
  show_calendar_link: true
cover:
  image: events/grid/scooby_sunday/SS0.1.png
---

## Grand Prix Circuits

Red Bull Ring
: 10 Laps

Sydney Motorsport Park: Gardner
: 10 Laps

---
**Back to Skirmish**

---
## City Circuits

Barcelona: Cathedral Pass
: 9 laps

Shanghai: The Oriental Pearl Circuit
: 12 laps
