---
title: Havana Nights
author: rainyKAROO
stadia: rainyKAROO#5277
tags: ["GRID"]
keywords: ["racing","event", "grid"]
description: "Chevrolet Camaro Z28 Modified"
expiryDate: 2021-05-16T19:30:00+00:00
layout: event
event:
  start: 2021-05-16T19:30:00+00:00
  duration: 90m
  show_calendar_link: true
cover:
  image: "havana_nights_flyer.png"
  alt: "Havana Nights Event Poster"
---

Enter some varying nightly vintage racing events at all 7 city tracks of Havana. 

All events will be done in one single session (each ~30 min.).
<!--more-->

Drivers will be asked to confirm their intent in [discord](/discord) to race, in order to give hosts time to prepare.

Invites will be sent at 21:15 CEST and we will start exactly at 21:30. Late joiners are welcome anyway. Please let me also know if you wanna join voice chat. As long as we're less then 10 we can use Stadia Party Chat.

So, if you wanna join do the click job below and we'll see us on track then...

---
## Car
Chevrolet Camaro Z28 Modified

---

## General Info

Hot Lap Qualifying
: No

Return to Skirmish
: No

Terminal Damage
: Full

---
# Sessions
## Session 1
**Havana Nights Sprint Cup**

- will be held at the 4 shortest track layouts / 5-6 laps each
- Grid positions will be determined by coincidence for race 1 and by reversed event standings for races 2-4
- no return to Skirmish

## Session 2
**Havana Nights Time Trial Trophy**

time trial mode (15 min.)

- El Malecón 
- Castillo View

## Session 3
**Havana Nights Main Race**

- Paseo de Marti, Grid positions will be determined by Hotlap
