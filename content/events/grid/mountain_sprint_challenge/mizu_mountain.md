---
title: Mizu Mountain - Okutama Sprint
author: Hellmonkey88
layout: event
tags: ["GRID", "Mountain Sprint Challenge"]
weight: 4
expiryDate: 2021-02-29T00:01:00+00:00
event:
    start: 2021-02-22T01:00:00+00:00
    end: 2021-02-28T23:00:00+00:00
    show_calendar_link: true
---