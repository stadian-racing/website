---
title: Sakura Pass - Okutama Sprint
author: Hellmonkey88
layout: event
tags: ["GRID", "Mountain Sprint Challenge"]
weight: 2
expiryDate: 2021-02-16T00:01:00+00:00
event:
    start: 2021-02-08T01:00:00+00:00
    end: 2021-02-15T23:00:00+00:00
    show_calendar_link: true
---