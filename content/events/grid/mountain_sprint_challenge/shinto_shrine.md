---
title: Shinto Shrine - Okutama Sprint
author: Hellmonkey88
layout: event
tags: ["GRID", "Mountain Sprint Challenge"]
weight: 3
expiryDate: 2021-02-22T00:01:00+00:00
event:
    start: 2021-02-15T01:00:00+00:00
    end: 2021-02-21T23:00:00+00:00
    show_calendar_link: true
---