---
title: Tenshi Way - Okutama Sprint
author: Hellmonkey88
tags: ["GRID", "Mountain Sprint Challenge"]
layout: event
weight: 1
expiryDate: 2021-02-08T00:01:00+00:00
event:
    start: 2021-02-01T01:00:00+00:00
    end: 2021-02-07T23:00:00+00:00
    show_calendar_link: true
---