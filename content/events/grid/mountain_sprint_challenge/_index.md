---
draft: false
title: Mountain Sprint Challenge
author: Hellmonkey88
expiryDate: 2021-03-01T01:00:00+00:00
keywords: ["racing","events", "grid", "challenge"]
tags: ["GRID", "Mountain Sprint Challenge"]
weight: 1
cover:
    image: MSC4.jpg
---

Category:
: Invitational

Class:
: Historic GT

Vehicle:
: Ford GT40
<!--more-->
----

## Point Scoring

Top 10 fastest times on each listed route as of midnight GMT on the 7th, 14th, 21st & 28th February will be scored as follows:

1st
: 10

2nd
: 9

3rd
: 8

4th
: 7

5th
: 6

6th
: 5

7th
: 4

8th
: 3

9th
: 2

10th
: 1

There is no set time for this event series. You have one week to set your fastest time on the designated route for that week and you can do it whenever you like so this should not interfere with any other events we have going.

Best of luck, you’ll need it in that Ford… Mwahahahaha!

## Tracks & Dates
----