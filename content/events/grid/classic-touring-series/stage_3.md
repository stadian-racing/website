---
title: Stage 3
author: Redfox
stadia: Redfox#2355
tags: ["GRID", "Classic Touring Series"]
description: BMW M3 Touring Car
expiryDate: 2021-04-21T22:00:00+00:00
layout: event
event:
  start: 2021-04-21T21:00:00+00:00
  duration: 2h
  show_calendar_link: true
---
## Vehicle 

**BMW M3 Touring Car**

## Races

HAV El Malecon
: Afternoon, 8 Laps

HAV Castilio
: Afternoon, 7 Laps

PAR Champs
: Dusk, 6 Laps

PAR Pont de L’alma
: Dusk, 10 Laps
