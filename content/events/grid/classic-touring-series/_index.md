---
title: Classic Touring Series
author: Redfox
stadia: Redfox#2355
tags: ["GRID", "Classic Touring Series"]
keywords: ["racing","event", "grid"]
description: "Alfa 155 and BMW M3"
expiryDate: 2021-05-01T22:00:00+00:00
---
April is Classic Touring Month for Wednesdays in the SRC!

Alfa 155 and BMW M3
<!--more-->

Each stage will alternate between street and track circuits.  

Each car will get 2 stages.

Up to 16 drivers; or 22-32 drivers.

If we get 22 or more drivers, we will run two lobbies, with promotion and relegation into divisions each week.

Drivers will be asked to confirm their intent in [discord](/discord) to race each week, by Tuesday afternoon, for races on the following Wednesday evening, in order to give hosts time to prepare.

---

## General Info

Hot Lap Qualifying
: Yes

Return to Skirmish
: No

Vehicle Damage
: No

Terminal Damage
: No

---

## Points

In-game points system will be used.

There will be 1 additional point awarded in each race for: Qualifying Pole & Fastest Lap

---