---
title: Stage 4
author: Redfox
stadia: Redfox#2355
tags: ["GRID", "Classic Touring Series"]
description: BMW M3 Touring Car
expiryDate: 2021-04-28T22:00:00+00:00
layout: event
event:
  start: 2021-04-28T21:00:00+00:00
  duration: 2h
  show_calendar_link: true
---
## Vehicle 

**BMW M3 Touring Car**

## Races

SYD Druitt 
: Day, 10 Laps

RBR North
: Day, 12 Laps

SHG Bund
: Night, 7 Laps

SHG West
: Night, 7 Laps
