---
title: Stage 1
author: Redfox
stadia: Redfox#2355
description: Alfa Romeo 155 TS
tags: ["GRID", "Classic Touring Series"]
expiryDate: 2021-04-07T22:00:00+00:00
layout: event
event:
  start: 2021-04-07T21:00:00+00:00
  duration: 2h
  show_calendar_link: true
---
## Vehicle 

**Alfa Romeo 155 TS**

## Races

SF Sprint
: Afternoon, 6 Laps

SF Short
: Afternoon, 10 Laps

BAR High Street
: Dusk, 10 Laps

BAR Marina
: Dusk, 6 Laps

