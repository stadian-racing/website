---
title: Stage 2
author: Redfox
stadia: Redfox#2355
description: Alfa Romeo 155 TS
tags: ["GRID", "Classic Touring Series"]
expiryDate: 2021-04-14T22:00:00+00:00
layout: event
event:
  start: 2021-04-14T21:00:00+00:00
  duration: 2h
  show_calendar_link: true
---
## Vehicle 

**Alfa Romeo 155 TS**

## Races

ZJG
: Day, 7 Laps

BH GP
: Day, 7 Laps

SUZ West Rev
: Night, 7 Laps

SYD Amaroo
: Night, 11 Laps