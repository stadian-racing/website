---
title: Classic Minis
author: Redfox
tags: ["GRID", "Slow and Steady"]
expiryDate: 2021-08-04T22:00:00+00:00
layout: event
event:
  start: 2021-08-04T20:00:00+00:00
  duration: 60m
  show_calendar_link: true
weight: 1
---
## Vehicle 

**Classic Mini Cup Mini Miglia**

---

## Races

BAR Torres
: Day, 27 Laps

BAR Fountain
: Afternoon, 17 Laps

ZJG East
: Night, 16 Laps
---

## General Info

Hot Lap Qualifying
: No

Return to Skirmish
: No

Vehicle Damage
: No

Terminal Damage
: No

Grid
: Reversed


> Reversed Grids to allow for lots of overtaking fun. Lots of laps, so no need to rush it in the early race. Enjoy the drive!