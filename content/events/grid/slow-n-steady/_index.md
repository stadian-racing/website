---
title: Slow and Steady Race Series 
author: Redfox
tags: ["GRID", "Slow and Steady"]
keywords: ["racing","event", "grid"]
description: "A themed series of individual race events, showcasing cars from slower classes, including those less popular in public lobbies"
expiryDate: 2021-09-09T22:00:00+00:00
cover:
  image: slow-n-steady.jpg
---

A themed series of individual race events, showcasing cars from slower classes, including those less popular in public lobbies. 

Each race day will have a specified car for us to explore and compete with.  The slower cars have often lent themselves to fun wheel to wheel moments; I look forward to sharing some with you.
<!--more-->

There are a handful of short tracks, so watch for back markers that could get recaptured in the action.

Weekly individual events. No series championship points.  Drop in for one raceday, or come play all 6 events as you wish.

Up to 16 drivers.

Drivers will be asked to confirm their intent in [discord](/discord) to race each week, by Tuesday afternoon, for races on the following Wednesday evening, in order to give hosts time to prepare team rosters.

---
### Format

Each race day will feature a specified car.  2 locations, for 3 races.
Reversed Grids.
Each race will be 15 minutes each; lots of time to show off your clean driving abilities, as the field sorts itself each race.

---

## Stages
