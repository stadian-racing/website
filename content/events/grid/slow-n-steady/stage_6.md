---
title: Subaru WRX STI
author: Redfox
tags: ["GRID", "Slow and Steady"]
expiryDate: 2021-09-08T22:00:00+00:00
layout: event
event:
  start: 2021-09-08T20:00:00+00:00
  duration: 60m
  show_calendar_link: true
weight: 5
---
## Vehicle 

**TC-2 Subaru WRX STI**

---

## Races

SEP North
: Day, 13 Laps

PAR Circuit de la Seine
: Overcast, 10 Laps

PAR Champs
: Afternoon, 14 Laps
---

## General Info

Hot Lap Qualifying
: No

Return to Skirmish
: No

Vehicle Damage
: No

Terminal Damage
: No

Grid
: Reversed


> Reversed Grids to allow for lots of overtaking fun. Lots of laps, so no need to rush it in the early race. Enjoy the drive!