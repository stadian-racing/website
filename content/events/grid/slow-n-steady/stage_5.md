---
title: Ford Sierra RS500 Cosworth
author: Redfox
tags: ["GRID", "Slow and Steady"]
expiryDate: 2021-09-01T22:00:00+00:00
layout: event
event:
  start: 2021-09-01T20:00:00+00:00
  duration: 60m
  show_calendar_link: true
weight: 5
---
## Vehicle 

**Classic Touring Ford Sierra RS500 Cosworth**

---

## Races

CV Club
: Dusk, 15 Laps

CV National 
: Night, 13 Laps

SEP South
: Day, 14 Laps
---

## General Info

Hot Lap Qualifying
: No

Return to Skirmish
: No

Vehicle Damage
: No

Terminal Damage
: No

Grid
: Reversed


> Reversed Grids to allow for lots of overtaking fun. Lots of laps, so no need to rush it in the early race. Enjoy the drive!