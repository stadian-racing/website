---
title: Nissan 350Z
author: Redfox
tags: ["GRID", "Slow and Steady"]
expiryDate: 2021-08-25T22:00:00+00:00
layout: event
event:
  start: 2021-08-25T20:00:00+00:00
  duration: 60m
  show_calendar_link: true
weight: 4
---
## Vehicle 

**GT-2 Nissan 350Z**

---

## Races

BH Indy Rev
: Day, 17 Laps

SF Short
: Dusk, 13 Laps

SF Sprint
: Night, 9 Laps
---

## General Info

Hot Lap Qualifying
: No

Return to Skirmish
: No

Vehicle Damage
: No

Terminal Damage
: No

Grid
: Reversed


> Reversed Grids to allow for lots of overtaking fun. Lots of laps, so no need to rush it in the early race. Enjoy the drive!