---
title: Modified Nissan 350ZX
author: Redfox
tags: ["GRID", "Slow and Steady"]
expiryDate: 2021-08-11T22:00:00+00:00
layout: event
event:
  start: 2021-08-11T20:00:00+00:00
  duration: 60m
  show_calendar_link: true
weight: 2
---
## Vehicle 

**Modified Nissan 350ZX**

---

## Races

ZJG Full
: Overcast, 9 Laps

SYD Amaroo
: Sunset, 15 Laps

SYD Druitt
: Dusk, 13 Laps
---

## General Info

Hot Lap Qualifying
: No

Return to Skirmish
: No

Vehicle Damage
: No

Terminal Damage
: No

Grid
: Reversed


> Reversed Grids to allow for lots of overtaking fun. Lots of laps, so no need to rush it in the early race. Enjoy the drive!