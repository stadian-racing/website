---
title: Chevrolet Camaro
author: Redfox
tags: ["GRID", "Slow and Steady"]
expiryDate: 2021-08-18T22:00:00+00:00
layout: event
event:
  start: 2021-08-18T20:00:00+00:00
  duration: 60m
  show_calendar_link: true
weight: 3
---
## Vehicle 

**Muscle Chevrolet Camaro SSX**

---

## Races

RBR North
: Day, 17 Laps

RBR South
: Afternoon, 16 Laps

BH Indy
: Night, 18 Laps
---

## General Info

Hot Lap Qualifying
: No

Return to Skirmish
: No

Vehicle Damage
: No

Terminal Damage
: No

Grid
: Reversed


> Reversed Grids to allow for lots of overtaking fun. Lots of laps, so no need to rush it in the early race. Enjoy the drive!