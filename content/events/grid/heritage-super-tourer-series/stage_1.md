---
title: Stage 1
author: Redfox
stadia: Redfox#2355
description: Ford Sierra RS 500 Cosworth Group A
tags: ["GRID", "Heritage Super Tourer Series"]
expiryDate: 2021-05-05T22:00:00+00:00
layout: event
event:
  start: 2021-05-05T20:00:00+00:00
  duration: 90m
  show_calendar_link: true
---
## Vehicle 

**Ford Sierra RS 500 Cosworth Group A**

## Races

RBR GP
: Day, 7 Laps

OKU GP 
: Sunset, 5 Laps

BAR High Street
: Sunset Rain, 9 Laps

IND Road Rev
: Dusk, 7 Laps