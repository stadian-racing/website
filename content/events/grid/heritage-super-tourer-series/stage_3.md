---
title: Stage 3
author: Redfox
stadia: Redfox#2355
description: Skyline GT-R  or Sierra Cosworth
tags: ["GRID", "Heritage Super Tourer Series"]
expiryDate: 2021-05-19T21:00:00+00:00
layout: event
event:
  start: 2021-05-19T20:00:00+00:00
  duration: 90m
  show_calendar_link: true
---
## Vehicle 

**Skyline GT-R  or Sierra Cosworth**

## Races

BH Indy
: Afternoon, 14 Laps

SYD Druitt
: Sunset, 11 Laps

RBR GP Rev
: Dusk, 7 Laps

SS National
: Night Rain, 11 Laps