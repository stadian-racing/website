---
title: Stage 2
author: Redfox
stadia: Redfox#2355
description: Nissan Skyline GT-R Group A R32
tags: ["GRID", "Heritage Super Tourer Series"]
expiryDate: 2021-05-12T21:00:00+00:00
layout: event
event:
  start: 2021-05-12T20:00:00+00:00
  duration: 90m
  show_calendar_link: true
---
## Vehicle 

**Nissan Skyline GT-R Group A R32**

## Races

BAR Columbus
: Day, 9 Laps

SHG Yan’an Tunnel
: Sunset, 6 Laps

BH GP Rev
: Sunset Rain, 7 Laps

IND Road
: Dusk, 8 Laps
