---
title: Heritage Super Tourer Series
author: Redfox
stadia: Redfox#2355
tags: ["GRID", "Heritage Super Tourer Series"]
keywords: ["racing","event", "grid"]
description: "Ford Sierra RS and Nissan Skyline GT-R"
expiryDate: 2021-06-01T22:00:00+00:00
cover:
    image: "hsts.jpg"
    alt: "Heritage Super Tourer Series poster"
---
The month of May will feature a Heritage Super Tourer Series in the SRC!

Up to 16 drivers.
<!--more-->

Drivers will be asked to confirm their intent in [discord](/discord) to race each week, by Tuesday afternoon, for races on the following Wednesday evening, in order to give hosts time to prepare team rosters.

---
### Cars
Ford Sierra RS 500 Cosworth Group A

Nissan Skyline GT-R Group A R32

---
### Format
#### Individual Drivers

Individual points will be tallied throughout Stages 1-4.

Points earned in Stages 1 & 2 will be used to build 2 balanced teams for Stages 3 & 4.

#### Teams
Stages 3 & 4 will additionally have team points tallied.

Each team will be assigned a car in Stage 3.  Each team will drive the opposite car in Stage 4.

---

## General Info

Hot Lap Qualifying
: Yes

Return to Skirmish
: No

Vehicle Damage
: No

Terminal Damage
: No

---

## Points

In-game points system will be used.

There will be 1 additional point awarded in each race for: Qualifying Pole & Fastest Lap

---