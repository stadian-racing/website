---
title: Stage 4
author: Redfox
stadia: Redfox#2355
description: Skyline GT-R  or Sierra Cosworth
tags: ["GRID", "Heritage Super Tourer Series"]
expiryDate: 2021-05-26T21:00:00+00:00
layout: event
event:
  start: 2021-05-26T20:00:00+00:00
  duration: 90m
  show_calendar_link: true
---
## Vehicle 

**Skyline GT-R  or Sierra Cosworth**

## Races

SS International
: Afternoon, 10 Laps

OKU GP Rev
: Sunset, 7 Laps

SYD Gardner
: Dusk, 7 Laps

SHG Nanpu
: Night Rain, 7 Laps
