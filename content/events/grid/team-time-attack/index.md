---
title: Team Time Attack Silvia S13 vs S15
author: Redfox
stadia: Redfox#2355
tags: ["GRID"]
keywords: ["racing","event", "grid"]
description: ""
expiryDate: 2021-02-24T22:00:00+00:00
layout: event
event:
    start: 2021-02-24T21:15:00+00:00
    duration: 2h
    show_calendar_link: true
---
## Cars

S13
: MCA Hammerhead Nissan Silvia

S15
: Nissan Silvia Time Attack Spec

<!--more-->
---
## General Info

Format
: Team/Race

Hot Lap Qualifying
: No

Return to Skirmish
: No

Vehicle Damage
: Yes, On

Terminal Damage
: Yes, On

---
## Race Calendar

Stage 1
: SEP GP, Day, 6 Laps

Stage 2
: OKU Grand, Day, 6 Laps

Stage 3
: IND Sport, Afternoon, 7 Laps

Stage 4
: SS 2009, Overcast, 7 Laps

---
## Points & Leaderboard
In-game points system will be used and calculated at the end of each round presented on a tracked leaderboard.

In-game points system will be used.

Drivers of the same car, are on the same team. 

Individual Driver points will be tracked.
