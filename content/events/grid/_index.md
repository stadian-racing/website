---
title: GRID
author: SRC Team
hideSummary: true
keywords: ["racing","events", "grid"]
weight: 3
gamelink: "https://stadia.google.com/store/details/ca805d3d37404654a91d2ea62fad7bd4rcp1/sku/75be643ff0fb40d7873fa41edb10d98a"
cover:
    image: "grid.jpg"
    alt: "GRID game poster"
    responsiveImages: true
    linkFullImages: true
---