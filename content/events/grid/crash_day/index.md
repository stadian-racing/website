---
title: "Crashday!"
author: KRAY
stadia: KRAY#2432
tags: ["GRID"]
keywords: ["racing","event", "grid"]
description: "The simple aim of the game is to crash and destroy all of your opponents while you try to survive the mass havoc and claim victory for the fastest lap of each round. No rules and No limits!"
expiryDate: 2021-04-17T20:00:00+00:00
layout: event
event:
    start: 2021-04-17T19:30:00+00:00
    duration: 90m
    show_calendar_link: true
    show_rules: false
cover:
    image: "crashday2021-04-19.png"
    alt: "Crashday poster"
---

The much loved, fun, no rules, crazy event makes a return after a very long absence from the race calendar! 

For players who don’t know what this is, Crashday is the event for all SRC members who play GRID to let loose, go crazy and have fun in fast world time attack cars on very short city circuits for up to 40 players. 

The only event where dirty racing and deliberate crashing is fully encouraged and expected! If you’re not crashing into the car next to you, then you’re racing too clean! Smash everyone so hard they get Terminal Damage!

This is one of the most favourite and highly anticipated events amongst many SRC members, sign up in the [discord](/discord) to secure your space, up to 40 players! 

Expect to receive broken steering, broken wheels, broken engines and broken dreams. Try to survive the madness! :pirate_flag:

Format
: Single/Time Attack/40 Players

Hot Lap Qualifying
: No

Return To Skirmish
: No

Vehicle Damage
: Yes/Full

Terminal Damage
: Yes

Points System
: In-game points system used

Required Car
: You can pick any car you want from the World Time Attack class. The choice is yours! 

---

## Race Schedule

> All tracks will be on a time-based format of 10 minutes per round.

Barcelona
: Fountain Loop

Havana
: El Capitolio

Paris
: Pont De LAlma

Shanghai
: The Oriental Peal

San Francisco
: Short Circuit B
