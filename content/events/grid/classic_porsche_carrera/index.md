---
title: "Classic Porsche 911 Carrera"
author: Redfox
tags: ["GRID"]
keywords: ["racing","event", "grid"]
description: "911 RSR Carrera RSR 3.0: One-off race event. 4 tracks. 1 car. Up to 16 drivers."
expiryDate: 2021-01-17T23:00:00+00:00
layout: event
event:
    start: 2021-01-17T21:15:00+00:00
    duration: 2h
    show_calendar_link: true
cover:
    image: "porsche.png"
    alt: "Classic Porsche 911 RSR poster"
---

Hot Lap Qualifying
: Yes

Return to Skirmish
: No

Vehicle Damage
: Visual

Terminal Damage
: No

---

## Race Schedule

Stage 1
: OKU Grand Sunset (5)

Stage 2
: IND Sport Rev Day (7)

Stage 3
: SEP North D. Rain (9)

Stage 4
: CV Infield Aft (6)

---

## Points & Leaderboard

In-game points system will be used and calculated at the end of each round presented on a tracked leaderboard.

There will be 1 additional point awarded in each race for:
 - Qualifying Pole
 - Fastest Lap

If you would like to take part in this event, please react below.
