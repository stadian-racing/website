---
title: Round 2
layout: event
author: Redfox
tags: ["GRID", "Classic Mini Mini Tournament"]
expiryDate: 2021-02-03T22:15:00+00:00
event:
  start: 2021-02-03T21:15:00+00:00
  duration: 2h
  show_calendar_link: true
---

Grid
: Top/Bottom 50% (Max 8) will be promoted/relegated

### Tracks
BH Indy
: 11 Laps 

Conditions
: Day
----
SUZ East
: 10 Laps

Conditions
: Afternoon
----
SEP South
: 8 Laps

Conditions
: Day Rain
----
ZJG East
: 11 Laps

Conditions
: Sun
