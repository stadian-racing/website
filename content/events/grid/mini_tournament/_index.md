---
title: Classic Mini Mini Tournament
author: RedfoxRedfox#2355
hideSummary: true
keywords: ["racing","events", "grid"]
expiryDate: 2021-02-10T22:15:00+00:00
tags: ["GRID"]
weight: 2
cover:
    image: MMC2.jpg
---

Car:
: Classic Mini Cup – Mini Miglia Challenge

Hot Lap Qualifying:
: Yes

Return to Skirmish:
: No

Vehicle Damage:
: Visual

Terminal Damage:
: No

<!--more-->

----
> In an effort to promote clean racing, the basic rules and etiquette expected can be viewed here: [rules of clean racing](https://www.stadianracing.com/rocr/)

## Points & Leaderboard
In-game points system will be used and calculated at the end of each round presented on a tracked leaderboard. Points will be reset each week for each round.

There will be 1 additional point awarded in each race for:
 - Qualifying Pole
 - Fastest Lap

Divisions
: Up to 32 Drivers

---- 

> If participant numbers exceed 16, will create divisions with promotion and relegation each week.


