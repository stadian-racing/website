---
title: Round 3
layout: event
author: Redfox
tags: ["GRID", "Classic Mini Mini Tournament"]
expiryDate: 2021-02-10T22:15:00+00:00
event:
  start: 2021-02-10T21:15:00+00:00
  duration: 2h
  show_calendar_link: true
---

Grid
: Top/Bottom 25% (Max 4) will be promoted/relegated

### Tracks
OKU Sprint
: 12 Laps 

Conditions
: Sun
----
RBR North
: 11 Laps

Conditions
: Day
----
AYD Amaroo Reversed
: 9 Laps

Conditions
: Day Rain
----
CV Club
: 10 Laps

Conditions
: Dusk
