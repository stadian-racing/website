---
title: Round 1
layout: event
author: Redfox
tags: ["GRID", "Classic Mini Mini Tournament"]
expiryDate: 2021-01-17T22:15:00+00:00
event:
  start: 2021-01-27T21:15:00+00:00
  duration: 2h
  show_calendar_link: true
---

Grid
: Random assignment

### Tracks
SF Short
: 9 Laps 

Conditions
: Afternoon
----
BAR Cathedral
: 9 Laps

Conditions
: Overcast
----
HAV El Capitolio
: 11 Laps

Conditions
: Sunset Rain
----
PAR Le Trocadero
: 9 Laps

Conditions
: Day
