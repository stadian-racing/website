---
title: King of the Redbull Ring
stadia: HellMonkey88#3687
tags: ["GRID"]
keywords: ["racing","event", "grid"]
expiryDate: 2021-03-06T22:00:00+00:00
layout: event
event:
    start: 2021-03-06T19:00:00+00:00
    duration: 2h
    show_calendar_link: true
---
**16 players** go *head to head* around the redbull ring.

**1 King** will be crowned.

<!--more-->
---
> For everyone's benefit, please ensure you've added HellMonkey88 on Stadia in good time prior to the event.

The host will open a private lobby and invite 2 drivers at a time to race. 

Race will be set to 3 laps. 

Since the host will have a vehicle on the grid they'll ask you to wait for them to retire to spectator, pull up to the start line and go on their say so. 

The grid position will be set to random so if the host is placed in P1 then P3 will take their spot on that side of the grid. 
Same if the host is placed P2. 

The host will be on comms to direct you if any unforeseen issues occur.

For this you will need to be able to hear game chat/party chat.

You will start on the hosts mark and race to the end of the 3 laps.

Winner progresses to next round.

---
## Race Information
*You have a free choice of vehicle in each car class*

Qualifiers
: GT2 Class

Quarter Finals
: GT1 class

Semi Finals
: Prototype

Finals
: Prototype

