---
title: DIRT 5
author: SRC Team
hideSummary: true
keywords: ["racing", "events", "dirt 5"]
weight: 4
cover:
  image: "D5_Main_Logo.png"
  alt: "DIRT 5 game poster"
  responsiveImages: true
  linkFullImages: true
---
