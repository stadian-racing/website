---
title: Social
aliases: ["trials-rising"]
keywords: ["racing","event", "trials rising"]
tags: ["Trials Rising", "Weekly"]
author: "bassforce86"
layout: "event"
stadia: bassforce86#0
event:
    start: 2021-01-09T21:00:00+00:00
    recurring: Weekly
    duration: 1h
    show_calendar_link: true
---

Come join us in our weekly mayhem!

We are currently just using the Global Multiplayer, due to the small number of concurrent online players.

When this gets busier we'll be running Lobbies!
