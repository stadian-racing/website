# CHANGELOG

<!--- next entry here -->

## 1.1.2
2021-08-12

### Fixes

- remove master from pages deploy (4775b8f88605a9944ca0d7b5564207952ce00840)

## 1.1.1
2021-08-12

### Fixes

- updated redbull ring race date (24cb04670ba8fe4e8140c3e70e1e3c4a6eb8a504)

## 1.1.0
2021-08-11

### Features

- added vgps2 (02bc69fece1d64bddc5704c5a85ff587e3aa140d)
- added xotic

## 1.0.3
2021-07-30

### Fixes

- allow release tagging to fail (d096edcda463fa65f899a5badbb6e4b93f253f64)

## 1.0.2
2021-07-29

### Fixes

- switched pages to build from master - not tag (f230799ec38d919d056dcfd96070c24087d987e0)

## 1.0.1
2021-07-29

### Fixes

- added tag pipeline (b62f7008f9d825088dcc688bcae22c8c5b82ade6)

## 1.0.0
2021-07-29

### Features

- updated rss to atom feeds (fad2ee787643002a329bdce249092039af502b5d)
- added duration & event info (487631366510e995265c4ff9b7a979ad28ca8623)
- updates theme to v5.0 (8552ab5e6e04d075de7b0561050625061403f144)
- update crash day (23250ea16f296b7af9c27d0a5b6cb6ce3403cb79)
- added hsts (65d73e0da739842ab0e4348903aca620680d17e4)
- add event (af6897017d0b44e3b33cf5575be6660dcd1532d9)
- added scooby sundays events (2730f335716462e6e8a79eb31b8ebb9b8bac935c)
- add hidden new rocr (6e2e049f348a03db9ab46983621a8620a0af031f)
- added virtual GP (ab38956df25585c2abee33b370603d789833ea53)
- add gt1 tour (1ad8406a2d55f472bc1b10ccba7ec96dbaa20186)
- added sundowner event (e199e7926ad47d169f25b0d48fd7ce897a5400dc)
- added collapsible element to archive (576853ef42e0b2aec725740d33a39165a88c5d87)
- added versioning to pipeline (51efd442eac7d3e51b1336cc459165b05075f954)

### Fixes

- updated team with stadia refs (b4652ef7107dc88185181a72c30ad21a1c047afb)
- added super stock series (8a2dab8dd2a06fbe8508f0ec2e36d5dd6d3a1de9)
- updated to only show past events (67b7708a247c92230fe30063c507b62cdedfe450)
- added cts april series (a39231580e9c78be68400ce28d9652d1e681ca0c)
- added series tags (14e528e1880dd4da8ac18b194bd89b73e3b88b40)
- converted f1 races from cet to utc (749b908f2a9bd7eedb54ac7e41b85d214292e799)
- added event poster (19e4073f8d38cf5ac95e4b02e2aeed999af42004)
- updated times (57f6e0bf1d45a4064e44b254efb8765e62be29e6)
- social tags (6d571bf2fbb9c31b8b6bc395a7813f1c0a7e74c6)
- added poster to page (7334eafbd25a170ae7f1fadce0151fed439f7b78)
- changed image to cover (a8c3a7a08d6396e204ecac721ec02beb30735659)
- updated virtual-gp info (4fdf52391598478f1f712f4a1b8c0d116ddf607a)
- updated event names (7d8875da7457e09c2445592ee842b504e6c2ca79)
- updated vgp times (ac04fb4955f899f01d9415e1804dbb82ef6052b8)
- added gt1 images (e3cb52f31020b55b9e89e96867380b24c16b5e23)
- add images (c863d4b6a04d4a6abe61e9ecc4b1ecb621f475b1)
- add weds f1 league (27d28b7aeaa829646390817df32a08ab718b77a4)
- replace rocr (4ca8ca7c88f7e1b7ae225c5149a87d5f423f264c)
- updated head to include redirect (dd8b0b95eb714cbae5c8adf024a402eac80c5ea3)
- added cover image (216c1f6ebfa1602696370040919e0f622e7b9753)
- changes base image (b02ebe5f48d73156f6317ad384dca45e9b4456fd)
- updated to auto-release (d57781ca9b615eb3bab5130677821f12a91b3257)
