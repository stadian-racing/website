---
# When your event is ready, set this to false and it will show
draft: true

# Name of the Event
title: "{{ replace .Name "-" " " | title }}"

# Date
# This page will show it was written. 
#
# Because of how the site works, 
# the date needs to be in this format in order for the page to show it correctly
date: {{ .Date }}

# Expiry Date
# This is when the event should move from the events list to the Archive
expiryDate: {{ .Date }}

# Tags
# This is more for SEO and helping people find things
# add / remove any tags that are/aren't relevant
tags: ["F1 2020", "Grid", "Trials Rising"] 

# Event Host(s)
#
# If you want to show multiple hosts write it as follows:
# ["host 1", "host 2", "host 3"] for example: ["Scanline", "Hellmonkey88"]
author: SRC Team

# Layout
#
# This tells the site that it's an event 
# so it shows the event panel
layout: event

# Event
event:
    # Start must be in this format. but can be any date & time.
    start: {{ .Date }}
    # This is exactly what will show in the Event Duration on the page
    duration: 1 hour
    # This can be used to set an interval between dates (bi weekly, bi monthly etc)
    interval: 1
    # This is the end date - if it's a multi-day event
    ends: {{ .Date }}
    # this displays a google calendar link for the event
    show_calendar_link: true
    # This can be used if the event is a recurring event
    recurring: Monthly

# Cover
#
# This is the Image to display with the Event. 
# You can remove this (and the bits underneath) if you don't want your event to have an image.
cover:
    # all images should be stored in assets/images/
    # we don't need the assets/images/ part for it to show on the site.
    image: "tracks/uk/silverstone.png"
    # This is a short description of what the image shows (for people that use screen readers - so partially sighted people mainly)
    alt: "Event Image"

# DO NOT REMOVE THESE 3 LINES!!!
---

This is all the stuff happening in the event! you can write what ever you want here. 

For Fancy formatting tools, the site uses a syntax (symbols) called [Markdown](https://www.markdownguide.org/getting-started/).
Go to the link to find out how it works and what you can do with it.