# Event Request
## For Game

(please add the label for the game i.e: ~game::GRID )

## Event Summary

(Summarize the event)

### Host

(This will probably be you, but if you don't want to you can request one of our hosts)

### Proposed Date and Time

(please include the timezone so we can ensure it's converted correctly for everyone!)

/label ~event