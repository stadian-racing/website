# Bug Report
## Summary

(Summarize the bug encountered concisely - please note, this is for the site ONLY)

## Steps to reproduce

(How one can reproduce the issue - this is very important)

1.
1.
1.

## What is the current bug behaviour?

(What actually happens)


## What is the expected correct behaviour?

(What you should see instead)


## Relevant logs and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.)


## Possible fixes

(If you can, link to the line of code that might be responsible for the problem)

/label ~bug